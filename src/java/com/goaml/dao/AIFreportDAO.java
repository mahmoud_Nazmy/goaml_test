/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FtFirst;
import com.goaml.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author maksoud
 */
public class AIFreportDAO {
     public List<FtFirst> getAIFtransactions (String clietid){
      Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FtFirst> vFtFirst = new ArrayList<FtFirst>();
         try {
              tx = session.beginTransaction();

            String Sql = "select * from FtFirst v where  V.CUSRECID=:CUSRECID ";
//                    + "and TO_DATE(v.FTPROCESSINGDATE,'MM/DD/YYYY')"
//                    + " between(TO_DATE(:start,'MM/DD/YYYY') and TO_DATE(:end,'MM/DD/YYYY') )";

            SQLQuery query = session.createSQLQuery(Sql);
//            query.setParameter("transNumber", transNumber);
           query.setParameter("CUSRECID", clietid);
//           query.setParameter("start", start);
//           query.setParameter("end", end);
            vFtFirst = query.setResultTransformer(Transformers.aliasToBean(FtFirst.class)).list();
            
            session.flush();
            tx.commit();
         } catch (Exception e) {
         }
     return vFtFirst;
     }
     
      public List<FtFirst> gettransfertransactions (String clietid,String start,String end){
      Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FtFirst> vFtFirst = new ArrayList<FtFirst>();
         try {
              tx = session.beginTransaction();

            String Sql = "select * from FtFirst v where  V.CUSRECID=:CUSRECID ";
//                    + "v.FTPROCESSINGDATE"
//                    + " between((:start) and(:end))";

            SQLQuery query = session.createSQLQuery(Sql);
//            query.setParameter("transNumber", transNumber);
           query.setParameter("CUSRECID", clietid);
//         query.setParameter("start", start);
//           query.setParameter("end", end);
            vFtFirst = query.setResultTransformer(Transformers.aliasToBean(FtFirst.class)).list();
            
            session.flush();
            tx.commit();
         } catch (Exception e) {
         }
     return vFtFirst;
     }
      public List<FtFirst> getcurrtransactions (String clietid,String transaction){
      Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FtFirst> vFtFirst = new ArrayList<FtFirst>();
         try {
              tx = session.beginTransaction();

            String Sql = "select * from FtFirst v where  V.CUSRECID=:CUSRECID and V.FTRECID=:transNumber ";

            SQLQuery query = session.createSQLQuery(Sql);

           query.setParameter("CUSRECID", clietid);
           query.setParameter("transNumber", transaction);

            vFtFirst = query.setResultTransformer(Transformers.aliasToBean(FtFirst.class)).list();
            
            session.flush();
            tx.commit();
         } catch (Exception e) {
         }
     return vFtFirst;
     }
     
}
