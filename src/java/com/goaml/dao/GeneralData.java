/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.Roles;
import com.goaml.model.Uploadingmenu;
import com.goaml.utils.HibernateUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class GeneralData implements Serializable {

    private TreeMap uploadMap;

    public TreeMap getFilesMap() {
        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            String Sql = "select * from UPLOADINGMENU";
            SQLQuery query = session.createSQLQuery(Sql);
            query.addEntity(Uploadingmenu.class);
            List list = query.list();
            uploadMap = new TreeMap();
            for (Object obj : list) {
                Uploadingmenu u = (Uploadingmenu) obj;
                String id = u.getId();
                String name = u.getFileName();
                uploadMap.put(name, id);
            }
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error getting Govs:::  " + e.getMessage());
        } finally {

            session.close();
        }
        return uploadMap;
    }

    public String getSubRepPath(String Key) {
        Session bSession = null;
        Transaction tx = null;
        String path = null;

        try {
            bSession = HibernateUtil.getSessionFactory().openSession();
            tx = bSession.beginTransaction();
            String sql = "select value from SYSPARAM where KEY=:Key";
            SQLQuery query = bSession.createSQLQuery(sql);
            query.setParameter("Key", Key);
            path = (String) query.uniqueResult();
            bSession.flush();
            tx.commit();

        } catch (Exception ex) {
            System.out.println("Error ::" + ex.getMessage());
        } finally {
            bSession.clear();
            bSession.close();
        }
        return path;
    }

    public String copyFile(String fileName, InputStream in, String destination) {
        String abolutePath=null;
        try {

            // write the inputStream to a FileOutputStream
            File f = new File(destination + fileName);
            OutputStream out = new FileOutputStream(f);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            abolutePath = f.getAbsolutePath();
            System.out.println("AbsolutePath::::   "+abolutePath);
            in.close();
            out.flush();
            out.close();

            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return abolutePath;
    }
 String zipFile;
    String name;

    public String Compress(String folder_name) {
        try {

            String sourceDirectory = folder_name;

            name = new File(sourceDirectory).getName();
            zipFile = "Output" + ".zip";

            System.out.println("sssssss:::: " + name);

            //create byte buffer
            byte[] buffer = new byte[1024];
            /*
             * To create a zip file, use
             *
             * ZipOutputStream(OutputStream out)
             * constructor of ZipOutputStream class.
             *  
             */

            //create object of FileOutputStream
            FileOutputStream fout = new FileOutputStream(zipFile);

            //create object of ZipOutputStream from FileOutputStream
            ZipOutputStream zout = new ZipOutputStream(fout);

            //create File object from directory name
            File dir = new File(sourceDirectory);

            //check to see if this directory exists
            if (!dir.isDirectory()) {
                System.out.println(sourceDirectory + " is not a directory");
            } else {
                File[] files = dir.listFiles();

                for (int i = 0; i < files.length; i++) {
                    System.out.println("Adding " + files[i].getName());

                    //create object of FileInputStream for source file
                    FileInputStream fin = new FileInputStream(files[i]);
                    zout.putNextEntry(new ZipEntry(files[i].getName()));

                    /*
                     * After creating entry in the zip file, actually
                     * write the file.
                     */
                    int length;

                    while ((length = fin.read(buffer)) > 0) {
                        zout.write(buffer, 0, length);
                    }
                    zout.closeEntry();
                    fin.close();
                }
            }

            zout.close();

            System.out.println("Zip file has been created!");
            return zipFile;
        } catch (IOException ioe) {
            System.out.println("IOException :" + ioe);
            return "errorrrrrrrrrrrrrrrrrr";
        }

    }
        public TreeMap getRolesMap() {
        Session session = null;
        Transaction tx = null;
        TreeMap rolesMap = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            String Sql = "select * from roles";
            SQLQuery query = session.createSQLQuery(Sql);
            query.addEntity(Roles.class);
            List list = query.list();
            rolesMap = new TreeMap();
            for (Object obj : list) {
                Roles r = (Roles) obj;
                BigDecimal id = r.getRoleid();
                String name = r.getRole();
                rolesMap.put(name, id);
            }
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error getting Govs:::  " + e.getMessage());
        } finally {

            session.close();
        }
        return rolesMap;
    }
        public String getSysParam(String Key) {
        Session bSession = null;
        Transaction tx = null;
        String path = null;

        try {
            bSession = HibernateUtil.getSessionFactory().openSession();
            tx = bSession.beginTransaction();
            String sql = "select value from SYSPARAM where KEY=:Key";
            SQLQuery query = bSession.createSQLQuery(sql);
            query.setParameter("Key", Key);
            path = (String) query.uniqueResult();
            bSession.flush();
            tx.commit();

        } catch (Exception ex) {
            System.out.println("Error ::" + ex.getMessage());
        } finally {
            bSession.clear();
            bSession.close();
        }
        return path;
    }
}
