/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.beans.UsersBean;
import com.goaml.encryptor.DesEncryp_Decrypt;
import com.goaml.model.FilesHis;
import com.goaml.model.Users;
import com.goaml.utils.Encryption;
import com.goaml.utils.HandelSession;
import com.goaml.utils.HibernateUtil;
import com.goaml.utils.JsfUtil;
import com.goaml.utils.PasswordProtector;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.GenericJDBCException;
import org.primefaces.context.RequestContext;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Mahmoud
 */
public class UserManage {

    private Users user;
    public Encryption pp;

    public UserManage() {
        pp = new Encryption();
    }

    public String getUser(String userName, String pass) throws Exception {
        Session session = null;
        //System.out.println("Pass::" + pass);
        try {
            session = HibernateUtil.getSessionFactory().openSession();
        } catch (ExceptionInInitializerError e) {
            e.printStackTrace();
            RequestContext.getCurrentInstance().update(":growl");
            JsfUtil.addErrorMessage("ExceptionInInitializerError \n" + e.getMessage());

            return "";
        } catch (NoClassDefFoundError e) {
            RequestContext.getCurrentInstance().update(":growl");
            JsfUtil.addErrorMessage("NoClassDefFoundError \n" + e.getMessage());
            return "";
        } catch (HibernateException e) {
            RequestContext.getCurrentInstance().update(":growl");
            JsfUtil.addErrorMessage("HibernateException \n" + e.getMessage());
            return "";
        } catch (Exception e) {
            RequestContext.getCurrentInstance().update(":growl");
            JsfUtil.addErrorMessage("Exception \n" + e.getMessage());
            return "";
        }
        Transaction tx;

        try {
            tx = session.beginTransaction();
        } catch (GenericJDBCException e) {
            RequestContext.getCurrentInstance().update(":growl");
            JsfUtil.addErrorMessage("GenericJDBCException \n" + e.getMessage());
            return "";
        }

        String sql = "select * from users where username=:user and password=:pass  ";

        SQLQuery query = session.createSQLQuery(sql);
        query.setParameter("user", userName);
//        pass=encryptPass(userName, pass);
        System.out.println(""+pass);
        query.setParameter("pass", pass);

        query.addEntity(Users.class);
        user = (Users) query.uniqueResult();
        session.flush();
        tx.commit();
        session.close();
        if (user == null) {

            JsfUtil.addErrorMessage("Wrong userName or password");
            RequestContext.getCurrentInstance().update(":growl");
            return "";
        } else {
            if (user.getPassword() == null) {
                return "./resetPass.xhtml?faces-redirect=true";
            }
            HandelSession hSession = new HandelSession();
            UsersBean userLog = (UsersBean) hSession.getAttribute("userBean");
            userLog.setUsers(user);
            return "./App/Main.xhtml?faces-redirect=true";

        }
    }

    public void saveUser(Users u) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(u);
            session.flush();
            tx.commit();
            //session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            Log.error(ex.getMessage());
//            Log.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
    
        public String encryptPass(String user , String pass) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx;
        String quota = null;
        try {
            tx = session.beginTransaction();
            String sql = "SELECT encryptPass(:user , :pass )  from dual";
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter("user", user);
            query.setParameter("pass", pass);
            quota = (String) query.uniqueResult();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
//            Log.error(e.getMessage());
//            Log.printStackTrace(e);
        } finally {
            session.close();
        }
        return quota;
    }
        
        public static void main(String[] args) {
        UserManage um = new UserManage();
            System.out.println(""+um.encryptPass("test", "ss"));
    }
        
}
