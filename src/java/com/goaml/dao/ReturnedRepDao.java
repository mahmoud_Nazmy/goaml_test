/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FilesHis;
import com.goaml.model.ReturnedRepData;
import com.goaml.model.ReturnedRepView;
import com.goaml.model.ReturnedReports;
import com.goaml.utils.HibernateUtil;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Mahmoud
 */
public class ReturnedRepDao {

    public List<ReturnedRepView> getReturnedReports(String userId) {
        Session session = null;
        Transaction tx = null;
        List<ReturnedRepView> returnedReportses = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            String SQL = "select r.REPORT_ID,r.RETURN_FROM,r.REASON,r.RETURNED_TO,\n"
                    + "r.DATE_TIME , f.REPORT_TYPE , f.REASON repReason, f.CUSNUMBER ,f.FILE_PATH\n"
                    + "from RETURNED_REPORTS r, FILES_HIS f\n"
                    + "where RETURNED_TO=:userId\n"
                    + "and r.REPORT_ID=f.ID";
            SQLQuery query = session.createSQLQuery(SQL);
            query.setParameter("userId", userId);
            query.setResultTransformer(Transformers.aliasToBean(ReturnedRepView.class));
            returnedReportses = query.list();
            session.flush();
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error::: " + ex.getMessage());
        } finally {
            session.close();
        }
        return returnedReportses;
    }

    public ReturnedRepData getReturnedReportsData(String repId) {
        Session session = null;
        Transaction tx = null;
        ReturnedRepData reportData = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            String SQL = "select * from RETUREDREPDATA where ID=:id";
            SQLQuery query = session.createSQLQuery(SQL);
            query.setParameter("id", repId);
            query.setResultTransformer(Transformers.aliasToBean(ReturnedRepData.class));
            reportData = (ReturnedRepData) query.uniqueResult();

            session.flush();
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error::: " + ex.getMessage());
        } finally {
            session.close();
        }
        return reportData;
    }
}
