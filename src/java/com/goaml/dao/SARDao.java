/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FtFirst;
import com.goaml.model.SARCustomer;
import com.goaml.utils.HibernateUtil;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Mahmoud
 */
public class SARDao {
    
    
        public SARCustomer getSarCustomer(String cusNumber) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        SARCustomer customer = new SARCustomer();
        try {
            tx = session.beginTransaction();

            String Sql = "select * from SAR_CUSTOMER where recid=:recid";

            SQLQuery query = session.createSQLQuery(Sql);
            query.setParameter("recid", cusNumber);
            query.setResultTransformer(Transformers.aliasToBean(SARCustomer.class));
            customer = (SARCustomer) query.uniqueResult();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error::" + e.getMessage());
        } finally {
            session.close();
        }
        return customer;
    }
    
}
