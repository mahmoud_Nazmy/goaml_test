/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FilesHis;
import com.goaml.model.FtFirst;
import com.goaml.model.ReportedTransactions;
import com.goaml.model.TellerTrans;
import com.goaml.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author CODO
 */
public class CreateReportDao {

    public CreateReportDao() {
    }

    public List<FtFirst> getOpenRegistr(String transNumber, String cusNumber) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FtFirst> vFtFirst = new ArrayList<FtFirst>();
        try {
            tx = session.beginTransaction();

            String Sql = "select * from FtFirst v where   V.FTRECID=:transNumber and V.CUSRECID=:CUSRECID";

            SQLQuery query = session.createSQLQuery(Sql);
            query.setParameter("transNumber", transNumber);
            query.setParameter("CUSRECID", cusNumber);
            vFtFirst = query.setResultTransformer(Transformers.aliasToBean(FtFirst.class)).list();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error::" + e.getMessage());
        } finally {
            session.close();
        }
        return vFtFirst;
    }

    public FtFirst getNewTrans(String transNumber, String cusNumber) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        FtFirst vFtFirst = new FtFirst();
        try {
            tx = session.beginTransaction();

            String Sql = "select * from FtFirst v where   v.FTRECID=:transNumber and v.CUSTOMER_NO=:CUSRECID";

            SQLQuery query = session.createSQLQuery(Sql);
            query.setParameter("transNumber", transNumber);
            query.setParameter("CUSRECID", cusNumber);
            query.setResultTransformer(Transformers.aliasToBean(FtFirst.class));
            vFtFirst = (FtFirst) query.uniqueResult();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error::" + e.getMessage());
        } finally {
            session.close();
        }
        return vFtFirst;
    }
    
    public TellerTrans getTellerTrans(String transNumber, String cusNumber) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        TellerTrans tellerTrans = new TellerTrans();
        try {
            tx = session.beginTransaction();

            String Sql = "select t.* from TELLER_TRANS t where t.TRANS_REF=:transRef and t.CUSTOMERNO=:cusNo";

            SQLQuery query = session.createSQLQuery(Sql);
            query.setParameter("transRef", transNumber);
            query.setParameter("cusNo", cusNumber);
            query.setResultTransformer(Transformers.aliasToBean(TellerTrans.class));
            tellerTrans = (TellerTrans) query.uniqueResult();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error::" + e.getMessage());
        } finally {
            session.close();
        }
        return tellerTrans;
    }

    public void saveFile(FilesHis f) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(f);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            Log.error(ex.getMessage());
//            Log.printStackTrace(ex);
        } finally {
            session.close();
        }
    }

    public void saveReportedTrans(ReportedTransactions rt) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(rt);
            session.flush();
            tx.commit();
//            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            Log.error(ex.getMessage());
//            Log.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
    
    
    
}
