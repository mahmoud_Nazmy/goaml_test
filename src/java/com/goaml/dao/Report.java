/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

/**
 *
 * @author Mahmoud
 */
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import com.goaml.conn.Database;
import java.io.IOException;
import java.io.OutputStream;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import org.apache.commons.io.IOUtils;

public class Report {

    HashMap hm = null;
    Connection con = null;
    String reportName;
    String repName;

    Report(HashMap parameterMap, Connection connection) {
       
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

//    public Report() {
//        //  System.setProperty ("java.awt.headless", "false");
//        setExtendedState(MAXIMIZED_BOTH);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        //   System.setProperty ("java.awt.headless", "false");
//
//    }
//
//    public Report(HashMap map) {
//        this.hm = map;
//        setExtendedState(MAXIMIZED_BOTH);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//
//    }
//
//    public Report(HashMap map, Connection con) {
//        this.hm = map;
//        this.con = con;
//        setExtendedState(MAXIMIZED_BOTH);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        setTitle("HDB Report Viewer");
//
//    }

    public void setReportName(String rptName) {
        this.reportName = rptName;
    }

    public void callReport() throws JRException, IOException {
        JasperPrint jasperPrint = generateReport();
        //   JasperPrint jasperPrint = generateEmptyDataSourceReport();
//       System.out.println("getCurrentInstance::::   "+FacesContext.getCurrentInstance());
//       System.out.println("getExternalContext::::   "+FacesContext.getCurrentInstance().getExternalContext());
//       System.out.println("getResponse::::   "+FacesContext.getCurrentInstance().getExternalContext().getResponse());
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//    System.out.println("httpServletResponse::::   "+httpServletResponse);
        ServletContext servletContext = (ServletContext) externalContext.getContext();

        httpServletResponse.addHeader("Content-disposition", "filename=report.pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            System.out.println("servletOutputStream::::   " + servletOutputStream);

            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();

            servletOutputStream.flush();
            servletOutputStream.close();

//        JRViewer viewer = new JRViewer(jasperPrint);
//        Container c = getContentPane();
//        c.add(viewer);
//        this.setVisible(true);
        }
    }

    public void callConnectionLessReport() throws IOException, JRException {

        JasperPrint jasperPrint = generateEmptyDataSourceReport();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        //ServletContext servletContext = (ServletContext) externalContext.getContext();

        httpServletResponse.addHeader("Content-disposition", "filename=report.pdf");

        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);

        servletOutputStream.flush();
        FacesContext.getCurrentInstance().responseComplete();

        //JRViewer viewer = new JRViewer(jasperPrint);
//        Container c = getContentPane();
//        c.add(viewer);
//        this.setVisible(true);

    }

    public void mm() throws IOException, JRException {
        JasperPrint jasperPrint = generateReport();
//        FacesContext context = FacesContext.getCurrentInstance();
//        HttpServletResponse response
//                = (HttpServletResponse) context.getExternalContext().getResponse();
//
//        // set correct content type
//         response.addHeader("Content-disposition", "filename=report.pdf");
//        response.setContentType("application/pdf");
//ServletOutputStream servletOutputStream = response.getOutputStream();
//        // get OutputStream
////        OutputStream stream = response.getOutputStream();
//        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
//// TODO: generate PDF and write the report to this output stream 
//
//        // mark response as completed
//        this.setVisible(true);
//        context.responseComplete();
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.pdf");

        FacesContext.getCurrentInstance().responseComplete();

        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        System.out.println("All done the report is done");
        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();

    }

    public void closeReport() {
        //jasperViewer.setVisible(false);
    }

    /**
     * this method will call the report from data source
     */
    public JasperPrint generateReport() {
        try {

            if (con == null) {
                try {
                    con = Database.getConnection();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            JasperPrint jasperPrint = null;
            if (hm == null) {
                hm = new HashMap();
            }

            jasperPrint = JasperFillManager.fillReport("E:\\Reports\\STRReport" + ".jasper", hm, con);
            JasperExportManager.exportReportToPdfFile(jasperPrint, "E:\\Reports\\" + "ss" + ".pdf");
            JasperExportManager.exportReportToPdf(jasperPrint);
            return jasperPrint;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * call this method when your report has an empty data source
     */
    public JasperPrint generateEmptyDataSourceReport() {
        try {
            JasperPrint jasperPrint = null;
            if (hm == null) {
                hm = new HashMap();
            }
            jasperPrint = JasperFillManager.fillReport(reportName + ".jasper", hm, new JREmptyDataSource());
            return jasperPrint;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

}
