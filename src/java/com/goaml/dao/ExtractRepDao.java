/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FilesHis;
import com.goaml.model.ReturnedReports;
import com.goaml.utils.HibernateUtil;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class ExtractRepDao {

    public List<FilesHis> getReports() {
        Session session = null;
        Transaction tx = null;
        List<FilesHis> filesHises = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            String SQL = "select * from FILES_HIS";
            SQLQuery query = session.createSQLQuery(SQL);
            query.addEntity(FilesHis.class);
            filesHises = query.list();
            session.flush();
            tx.commit();

        } catch (Exception ex) {
            ex.getStackTrace();
            ex.printStackTrace();
            System.out.println("Error::: " + ex.getMessage());
        } finally {
            session.close();
        }
        return filesHises;
    }

    public void updateReportFlag(FilesHis f) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(f);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            Log.error(ex.getMessage());
//            Log.printStackTrace(ex);
        } finally {
            session.close();
        }
    }

    public void saveReturn(ReturnedReports returnedReports) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(returnedReports);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            Log.error(ex.getMessage());
//            Log.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
}
