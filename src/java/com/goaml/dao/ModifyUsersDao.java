/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.CustomersHdb;
import com.goaml.model.Users;
import com.goaml.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class ModifyUsersDao {
        public void saveUser(Users users) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(users);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
}
