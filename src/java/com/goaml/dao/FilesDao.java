/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.logger.LogControl;
import com.goaml.model.Accounts;
import com.goaml.model.CustomersHdb;
import com.goaml.model.FtTrans;
import com.goaml.model.TtTrans;
import com.goaml.model.UploadedFiles;
import com.goaml.utils.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class FilesDao {

    final static Logger logger = Logger.getLogger(LogControl.class);

    public void saveFile(CustomersHdb customersHdb) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(customersHdb);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }

    public void saveAccountsFile(Accounts accounts) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(accounts);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
      public void saveFTFiles(FtTrans ftTrans) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(ftTrans);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
         public void saveTTfile(TtTrans ttTrans) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(ttTrans);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
}
