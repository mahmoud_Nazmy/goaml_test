/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.model.FilesHis;
import com.goaml.model.UploadedFiles;
import com.goaml.utils.HibernateUtil;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class ReviseFilesDao {
    
    
        public List<UploadedFiles> getUploadedFiles() {
        Session session = null;
        Transaction tx = null;
        List<UploadedFiles> uploadedFileses = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            String SQL = "select * from UPLOADED_FILES";
            SQLQuery query = session.createSQLQuery(SQL);
            query.addEntity(UploadedFiles.class);
            uploadedFileses = query.list();
            session.flush();
            tx.commit();

        } catch (Exception ex) {
            ex.getStackTrace();
            ex.printStackTrace();
            System.out.println("Error::: " + ex.getMessage());
        } finally {
            session.close();
        }
        return uploadedFileses;
    }
}
