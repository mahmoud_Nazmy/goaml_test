/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.dao;

import com.goaml.logger.LogControl;
import com.goaml.model.FilesHis;
import com.goaml.model.UploadedFiles;
import com.goaml.utils.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mahmoud
 */
public class UploadFilesDao {
     	final static Logger logger = Logger.getLogger(LogControl.class);

    
        public void saveFile(UploadedFiles files) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(files);
            session.flush();
            tx.commit();
            session.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error:::   " + ex.getMessage());
//            LogControl.error(ex.getMessage());
//            LogControl.printStackTrace(ex);
        } finally {
            session.close();
        }
    }
}
