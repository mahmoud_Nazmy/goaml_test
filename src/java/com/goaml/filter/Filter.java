/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.filter;

/**
 *
 * @author Mahmoud
 */

import com.goaml.beans.UsersBean;
import com.goaml.model.Users;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Mahmoud
 */
public class Filter implements javax.servlet.Filter{
    private FilterConfig fc;
    private String timeoutPage = "GoAML/login.xhtml";    

 
      public String getTimeoutPage() {
        return timeoutPage;
    }

    public void setTimeoutPage(String timeoutPage) {
        this.timeoutPage = timeoutPage;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.fc = filterConfig; 
       
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        request.setCharacterEncoding("UTF-8");
        HttpSession session = ((HttpServletRequest) request).getSession(true);
        UsersBean userLog = (UsersBean) session.getAttribute("userBean");
        Users user = null;
        if (userLog != null) {
            user = userLog.getUsers();
        }
        if (user != null) {
            // User is logged in, so just continue request.
            if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
                HttpServletRequest httpServletRequest = (HttpServletRequest) request;
                HttpServletResponse httpServletResponse = (HttpServletResponse) response;

                if (isSessionControlRequiredForThisResource(httpServletRequest)) {
                    if (isSessionInvalid(httpServletRequest)) {
                        String timeoutUrl = httpServletRequest.getContextPath() + "/" + getTimeoutPage();
                        System.out.println("Session is invalid! redirecting to timeoutpage : " + timeoutUrl);
                        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/GoAML/login.xhtml");
                    } else {
                        chain.doFilter(request, response);
                    }
                }
            }
        } else {
            // User is not logged in, so redirect to index.
            HttpServletResponse res = (HttpServletResponse) response;
            res.sendRedirect(req.getContextPath() + "/GoAML/login.xhtml");
        }
    }

  @Override
    public void destroy() {
    }
    
       private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {
        String requestPath = httpServletRequest.getRequestURI();
        boolean controlRequired = !StringUtils.contains(requestPath, getTimeoutPage());
        return controlRequired;
    }

    private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {
        boolean sessionInValid = //(httpServletRequest.getRequestedSessionId() != null)&&
                !httpServletRequest.isRequestedSessionIdValid();
        return sessionInValid;
    }
}
