/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.UserManage;
import com.goaml.model.Users;
import com.goaml.utils.DirectoryWatchDemo;
import com.goaml.utils.JsfUtil;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "userBean")
@SessionScoped
public class UsersBean {

    private String userName;
    private String password;
    private Users users;
    private String render;

    public UsersBean() {
//        DirectoryWatchDemo watchDemo = new DirectoryWatchDemo();
//        watchDemo.watchService();

    }

    public Users getUsers() {

        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public UserManage getUserManage() {
        return userManage;
    }

    public void setUserManage(UserManage userManage) {
        this.userManage = userManage;
    }

    private UserManage userManage = new UserManage();

    public String logout() {
        try {

            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        } catch (NullPointerException exception) {
            JsfUtil.addErrorMessage(exception.getMessage());
        } catch (Exception exception) {
            JsfUtil.addErrorMessage(exception.getMessage());
        } finally {
            return "/login.xhtml?faces-redirect=true";
        }
    }

    public String validateUser() throws Exception {

        String result = userManage.getUser(userName, password);

        return result;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRender() {
        if (users.getRoleId().toString().equalsIgnoreCase("2")) {
            RequestContext.getCurrentInstance().execute("document.getElementById('users').style.display = 'none'");
            RequestContext.getCurrentInstance().execute("document.getElementById('Reports').style.display = 'none'");
            RequestContext.getCurrentInstance().execute("document.getElementById('Data').style.display = 'none'");
        }
        if (users.getRoleId().toString().equalsIgnoreCase("1")) {
            RequestContext.getCurrentInstance().execute("document.getElementById('users').style.display = 'none'");
            RequestContext.getCurrentInstance().execute("document.getElementById('ExtractReports').style.display = 'none'");
            RequestContext.getCurrentInstance().execute("document.getElementById('Data').style.display = 'none'");
            //RequestContext.getCurrentInstance().execute("document.getElementById('SystemReports').style.display = 'none'");

        }
        if (users.getRoleId().toString().equalsIgnoreCase("4")) {

            RequestContext.getCurrentInstance().execute("document.getElementById('ExtractReports').style.display = 'none'");
            RequestContext.getCurrentInstance().execute("document.getElementById('Reports').style.display = 'none'");

        }
            render = "true";
        
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

}
