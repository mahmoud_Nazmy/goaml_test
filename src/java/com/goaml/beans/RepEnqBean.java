/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.reports.AbstractReportBean;
import com.goaml.utils.HandelSession;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "RepEnqBean")
@ViewScoped
public class RepEnqBean extends AbstractReportBean {

    private String repId;
    private String repFromDate;
    private String repToDate;
    private String repType;
    private String inputter;
    UsersBean user;

    public RepEnqBean() {
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
    }

    public String execute() {
        try {
            super.prepareReport("RepEnq");
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }

        return null;
    }

    protected Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();

        reportParameters.put("RepId", repId);
        reportParameters.put("ReportType", repType);
        reportParameters.put("Inputter", inputter);
        reportParameters.put("DateTime", repFromDate);
        reportParameters.put("toDate", repToDate);
        reportParameters.put("UserName", user.getUserName());

        return reportParameters;
    }

    @Override
    protected String getCompileFileName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepFromDate() {
        return repFromDate;
    }

    public void setRepFromDate(String repFromDate) {
        this.repFromDate = repFromDate;
    }

    public String getRepType() {
        return repType;
    }

    public void setRepType(String repType) {
        this.repType = repType;
    }

    public String getInputter() {
        return inputter;
    }

    public void setInputter(String inputter) {
        this.inputter = inputter;
    }

    public String getRepToDate() {
        return repToDate;
    }

    public void setRepToDate(String repToDate) {
        this.repToDate = repToDate;
    }

}
