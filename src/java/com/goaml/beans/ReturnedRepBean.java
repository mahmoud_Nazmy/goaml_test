/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.GeneralData;
import com.goaml.dao.ReturnedRepDao;
import com.goaml.model.ReturnedRepView;
import com.goaml.reports.AbstractReportBean;
import com.goaml.utils.HandelSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "returnedRep")
@ViewScoped
public class ReturnedRepBean extends AbstractReportBean{

    UsersBean user = null;
    ReturnedRepDao dao;
    private List<ReturnedRepView> returnedReportses = new ArrayList<ReturnedRepView>();
    private ReturnedRepView selectedReport;
    GeneralData gd;

    public ReturnedRepBean() {
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
        dao = new ReturnedRepDao();
        returnedReportses = dao.getReturnedReports(user.getUsers().getUsername());
         gd = new GeneralData();
    }

    public List<ReturnedRepView> getReturnedReportses() {
        return returnedReportses;
    }

    public void setReturnedReportses(List<ReturnedRepView> returnedReportses) {
        this.returnedReportses = returnedReportses;
    }
    public void onRowSelect(SelectEvent event) {

        selectedReport = (ReturnedRepView) event.getObject();

    }

    public void onRowUnselect(UnselectEvent event) {
        this.selectedReport = null;

    }

    public ReturnedRepView getSelectedReport() {
        return selectedReport;
    }

    public void setSelectedReport(ReturnedRepView selectedReport) {
        this.selectedReport = selectedReport;
    }
    protected Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();
        if (selectedReport.getREPORT_TYPE().equalsIgnoreCase("STR")) {
            String subRepPath = gd.getSubRepPath("SubReportsPath");

            reportParameters.put("reportNo", selectedReport.getREPORT_ID());
            reportParameters.put("SUBREPORT_DIR", subRepPath);
        } else {
            reportParameters.put("reportNo", selectedReport.getREPORT_ID());
        }

        return reportParameters;
    }

    public String execute() {
        try {
            super.prepareReport(selectedReport.getREPORT_TYPE());
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }

        return null;
    }
    @Override
    protected String getCompileFileName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
