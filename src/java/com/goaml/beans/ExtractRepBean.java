/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.ExtractRepDao;
import com.goaml.dao.GeneralData;
import com.goaml.logger.Log;
import com.goaml.model.FilesHis;
import com.goaml.model.ReturnedReports;
import com.goaml.reports.AbstractReportBean;
import com.goaml.utils.HandelSession;
import com.goaml.utils.JsfUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "extractBean")
@ViewScoped
public class ExtractRepBean extends AbstractReportBean {

    private String returnReason;
    private FilesHis selectedFile;
    UsersBean user = null;
    ExtractRepDao dao;
    private List<FilesHis> files = new ArrayList<FilesHis>();
    private DefaultStreamedContent download;
    private DefaultStreamedContent downloadLibarary;
    private String filePath;
    private String libPath;
    GeneralData gd;
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("resources/messages");

    public ExtractRepBean() {
         Log.initialize("E:\\ListnerTest\\mm.log");
        dao = new ExtractRepDao();
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
        files = dao.getReports();
        gd = new GeneralData();
    }

    public DefaultStreamedContent getDownload() {
        try {
            System.out.println("Data::::::");
            filePath = selectedFile.getFilePath();
            System.out.println("FilePath:::  " + filePath);
//            String fullFilePath = unAuthorizedPath + "//" + fileName;

            File file2 = new File(filePath);
            InputStream input = new FileInputStream(filePath);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file2.getName()), file2.getName()));
            selectedFile.setStatus("O");
            dao.updateReportFlag(selectedFile);
        } catch (java.io.FileNotFoundException e) {
            e.printStackTrace();
            e.getMessage();
            JsfUtil.addErrorMessage("File Not Exsist");
        } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
            JsfUtil.addErrorMessage("Please Select file to download");
        }

        return download;
    }

    public DefaultStreamedContent getDownloadLibarary() throws FileNotFoundException {
        System.out.println("Data::::::");
        libPath = selectedFile.getFolderpath();
        System.out.println("FilePath:::  " + libPath);
        String rarPath = gd.Compress(libPath);
//            String fullFilePath = unAuthorizedPath + "//" + fileName;
        File file2 = new File(rarPath);
        InputStream input = new FileInputStream(rarPath);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownloadLibarary(new DefaultStreamedContent(input, externalContext.getMimeType(file2.getName()), file2.getName()));
        return downloadLibarary;
    }

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public void onRowSelect(SelectEvent event) {
        System.out.println("Hhhhhhhhhhhhhh");
        selectedFile = (FilesHis) event.getObject();

    }

    public void onRowUnselect(UnselectEvent event) {
        selectedFile = null;

    }

    public FilesHis getSelectedFile() {
        return selectedFile;
    }

    public void setSelectedFile(FilesHis selectedFile) {
        this.selectedFile = selectedFile;
    }

    public List<FilesHis> getFiles() {

        return files;
    }

    public void setFiles(List<FilesHis> files) {
        this.files = files;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void returnToUser(ActionEvent actionEvent) {
        System.out.println(""+selectedFile.getId());
        String toUser = selectedFile.getInputter();
        String repId = selectedFile.getId();
        ReturnedReports returnedReports = new ReturnedReports();
        returnedReports.setReportId(repId);
        returnedReports.setReturnedTo(toUser);
        returnedReports.setReason(returnReason);
        returnedReports.setReturnFrom(user.getUserName());
        try {
            dao.saveReturn(returnedReports);
            JsfUtil.addSuccessMessage(bundle.getString("reportReturnedToUser"));
            RequestContext.getCurrentInstance().execute("PF('dlg3').hide()();");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    protected Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();
        if (selectedFile.getReportType().equalsIgnoreCase("STR")) {
            String subRepPath = gd.getSubRepPath("SubReportsPath");

            reportParameters.put("reportNo", selectedFile.getId());
            reportParameters.put("SUBREPORT_DIR", subRepPath);
        } else {
            reportParameters.put("reportNo", selectedFile.getId());
        }

        return reportParameters;
    }

    public String execute() {
        try {
            super.prepareReport(selectedFile.getReportType());
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected String getCompileFileName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDownloadLibarary(DefaultStreamedContent downloadLibarary) {
        this.downloadLibarary = downloadLibarary;
    }

    public String getLibPath() {
        return libPath;
    }

    public void setLibPath(String libPath) {
        this.libPath = libPath;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

}
