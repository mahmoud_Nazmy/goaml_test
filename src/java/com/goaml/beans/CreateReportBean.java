/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.CreateReportDao;
import com.goaml.dao.GeneralData;
import com.goaml.model.FilesHis;
import com.goaml.model.FtFirst;
import com.goaml.model.ReportedTransactions;
import com.goaml.model.TellerTrans;
import com.goaml.utils.HandelSession;
import com.goaml.utils.JsfUtil;
import com.goaml.utils.XmlUtils;
import com.goaml.xml.Address;
import com.goaml.xml.Location;
import com.goaml.xml.ReportingPerson;
import com.goaml.xml.Transaction;
import com.goaml.xml.addresses;
import com.goaml.xml.from_person;
import com.goaml.xml.identification;
import com.goaml.xml.phone;
import com.goaml.xml.phones;
import com.goaml.xml.report;
import com.goaml.xml.report_indicators;
import com.goaml.xml.signatory;
import com.goaml.xml.t_from;
import com.goaml.xml.t_from_my_client;
import com.goaml.xml.t_person;
import com.goaml.xml.t_to_my_client;
import com.goaml.xml.to_account;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author CODO
 */
@ManagedBean(name = "ReportBean")
@ViewScoped
public class CreateReportBean {

    /**
     * Creates a new instance of CreateReportBean
     */
    CreateReportDao reportDao;
    UsersBean user = null;
    FilesHis fh;
    private String repId;
    private String savePath;
    ReportedTransactions rt;
    GeneralData gd;
    String xmlOutput;
    private UploadedFile fileName;
    String tempPath;
    String uploadedFileName;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH.mm.ss");
    List<String> attatchemnts;
    private FtFirst selecttedCase;
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("resources/messages");
    XmlUtils xmlUtils;

    public CreateReportBean() {
        reportDao = new CreateReportDao();
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
//        rt = new ReportedTransactions();
        gd = new GeneralData();
        FtFirst selecttedCase = new FtFirst();

        xmlUtils = new XmlUtils();
    }

    private String transNumber;
    private String cusNumber;
    private String action;
    private String reason;
    private List<FtFirst> FirstLS = new ArrayList<FtFirst>();
    private List<TellerTrans> tellerTransList = new ArrayList<TellerTrans>();
    FtFirst newTrans;
    TellerTrans tellerTrans;

    public void addToTransList(ActionEvent event) throws IOException {
        newTrans = new FtFirst();
        tellerTrans = new TellerTrans();
        attatchemnts = new ArrayList<>();
        attatchemnts.add(tempPath);
     
        String transactionType = transNumber.substring(0, 1);
        if (transactionType.equalsIgnoreCase("FT")) {
            newTrans = reportDao.getNewTrans(transNumber, cusNumber);
            if (newTrans == null) {
                JsfUtil.addErrorMessage(bundle.getString("SorryNoTrans"));
            }
            FirstLS.add(newTrans);
            transNumber = null;
            cusNumber = null;
        } else if (transactionType.equalsIgnoreCase("TT")) {
            tellerTrans = reportDao.getTellerTrans(transNumber, cusNumber);
            if (newTrans == null) {
                JsfUtil.addErrorMessage(bundle.getString("SorryNoTrans"));
            }
            
            
            tellerTransList.add(tellerTrans);
            transNumber = null;
            cusNumber = null;
        }

//        action = null;
//        reason = null;
    }

    public void onRowSelect(SelectEvent event) {
        System.out.println("Hhhhhhhhhhhhhh");
        selecttedCase = (FtFirst) event.getObject();

    }

    public void removeTrans() {
        FirstLS.remove(selecttedCase);
        System.out.println("transa Removed:::::");
    }

    public List<FtFirst> getFirstLS() {

        return FirstLS;
    }

    public void setFirstLS(List<FtFirst> FirstLS) {
        this.FirstLS = FirstLS;
    }

    public String getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(String transNumber) {
        this.transNumber = transNumber;
    }

    public String getCusNumber() {
        return cusNumber;
    }

    public void setCusNumber(String cusNumber) {
        this.cusNumber = cusNumber;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Success! ", event.getFile().getFileName() + " is uploaded.");
        uploadedFileName = event.getFile().getFileName();
        FacesContext.getCurrentInstance().addMessage(null, msg);
        try {
            tempPath = gd.copyFile(event.getFile().getFileName(), event.getFile().getInputstream(), gd.getSubRepPath("XMLOutput"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void scenario3() throws IOException {
        String num = "100017";
        repId = num.concat(JsfUtil.generatePIN());
        fh = new FilesHis();
        xmlOutput = gd.getSubRepPath("XMLOutput") + repId;

    }

    public void getData() throws IOException {
        String num = "100017";
        repId = num.concat(JsfUtil.generatePIN());
        fh = new FilesHis();
        xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
        System.out.println("Medooooooooooooooo");
        List<FtFirst> trans = FirstLS;
        List<ReportedTransactions> reList = new ArrayList<>();
        phones phoness = new phones();
        Address a = new Address();
        addresses add = new addresses();
//        Location location = new Location();
        phone phone = new phone();
        List<Transaction> tranList = new ArrayList<>();
        List<Address> address = new ArrayList<>();
        List<phone> phoneLis = new ArrayList<>();
        List<addresses> addressList = new ArrayList<>();
        List<phones> phonesList = new ArrayList<>();
        List<Location> locationList = new ArrayList<>();
        List<report_indicators> report_indicators = new ArrayList<>();
        Transaction t = null;
//        List<FtFirst> trans = FirstLS;
        System.out.println("ListSizeeee::::  " + trans.size());
//        Readxml(FirstLS);

        /////////////Report Header/////////////
        report r = xmlUtils.createRepHeader(repId, reason, action, "STR");
        ReportingPerson reportingPerson = new ReportingPerson();
        //////phoness///////
        phone.setTph_contact_type("P");
        phone.setTph_communication_type("M");
        phone.setTph_number(user.getUsers().getPhone());
        phoneLis.add(phone);
        phoness.setPhone(phoneLis);
        phonesList.add(phoness);
        ///////addressList////////
        a.setAddress_type("B");
        a.setAddress(user.getUsers().getAddress());
        a.setCity("Jiza");
        a.setCountry_code("EG");
        a.setState("Cairo");
        a.setAddress(user.getUsers().getBranch());
        address.add(a);
        add.setAddress(address);
        addressList.add(add);
        ///////////Location/////////////
        String Address_type = "B";
        String Address = "وسط البلد";
        String city = "Cairo";
        String country_code = "EG";
        String state = user.getUsers().getBranch();
        Location location = xmlUtils.createLocation(Address_type, Address, city, country_code, state);
//        location.setAddress_type("B");
//        location.setAddress("وسط البلد");
//        location.setCity("Cairo");
//        location.setCountry_code("EG");
//        location.setState("القاهرة");
        locationList.add(location);
        r.setLocation(locationList);
        //////////reportingPerson///////
        reportingPerson.setAddresses(addressList);
        reportingPerson.setGender(user.getUsers().getGender().substring(0, 1));
        reportingPerson.setTitle(user.getUsers().getTitle());
        reportingPerson.setFirst_name(user.getUsers().getFirstName());
        reportingPerson.setLast_name(user.getUsers().getLastName());
//        String year=user.getUsers().getBirthDate().substring(0, 4);
//        String month=user.getUsers().getBirthDate().substring(5, 6);
//        String day=user.getUsers().getBirthDate().substring(7, 8);
        String formattedDate = xmlUtils.formatDate(user.getUsers().getBirthDate());
        reportingPerson.setBirthdate(formattedDate);

        reportingPerson.setSsn(user.getUsers().getNationalId());
        reportingPerson.setAddresses(addressList);
        reportingPerson.setPhones(phonesList);
        reportingPerson.setEmail(user.getUsers().getEmail());
        reportingPerson.setOccupation(user.getUsers().getOccupation());
        ////////Indicators//////////
        report_indicators indicator = new report_indicators();
        List<String> indicators = new ArrayList<>();
        indicators.add("CC");
        indicators.add("CIUR");
        indicator.setIndicator(indicators);
        report_indicators.add(indicator);
        r.setReport_indicators(report_indicators);

        ////////Transactions////////
        for (int i = 0; i < trans.size(); i++) {
            t = new Transaction();
            t.setTransactionnumber(trans.get(i).getFTRECID());
            t.setTransaction_location(trans.get(i).getFT_BRANCH());
            t.setTransaction_description(trans.get(i).getDESCRRIPTION());
            t.setDate_transaction(trans.get(i).getPROCESSING_DATE());
            t.setTeller(trans.get(i).getINPUTTER());
            t.setAuthorized(trans.get(i).getAUTHORISER());
            t.setAmount_local(String.valueOf(trans.get(i).getDEBIT_AMOUNT()));
            //////////T_From////////////
//            List<ReportingPerson>fromPersonList = new ArrayList<>();
//            from_person fromPerson = new from_person();
//            t_from  t_from = new t_from();
//            t_from.setFrom_funds_code("C");
//            t_from.setFrom_country("EG");
//            t.setT_from(t_from);
//            fromPerson.setFirst_name(trans.get(i).getCUSSHORT_NAME());
//            fromPerson.setGender("Male");
//            fromPerson.setTitle("MR");
//            fromPersonList.add(fromPerson);
//            t_from.setFrom_person(fromPersonList);
            /////////////////////from_my_client/////////
            List<from_person> t_from_my_clientList = new ArrayList<>();

            from_person from_my_client = new from_person();

            //////phoness///////
            phone phoneClient = new phone();
            List<phone> phoneLisClient = new ArrayList<>();
            phones phonessClient = new phones();
            List<phones> phonesListClient = new ArrayList<>();
            phoneClient.setTph_contact_type("M");
            phoneClient.setTph_communication_type("P");
            phoneClient.setTph_number("01000000");
            from_my_client.setBirthdate("1984-11-16T00:00:00");
            from_my_client.setSsn(trans.get(i).getLEGAL_ID());
            from_my_client.setNationality1("EG");
            from_my_client.setResidence("EG");
            phoneLisClient.add(phoneClient);
            phonessClient.setPhone(phoneLisClient);
            phonesListClient.add(phonessClient);
            from_my_client.setPhones(phonesListClient);
            //////////addressList////////
            Address addressClient = new Address();
            addresses addressesClient = new addresses();
            List<Address> addressClientList = new ArrayList<>();
            List<addresses> addressListClient = new ArrayList<>();
            addressClient.setAddress_type("B");
            addressClient.setAddress(user.getUsers().getAddress());
            addressClient.setCity("City");
            addressClient.setCountry_code("EG");
            addressClient.setTown("Jizaa");
            addressClient.setState("Cairo");
            addressClient.setAddress("address");
            addressClientList.add(addressClient);
            addressesClient.setAddress(addressClientList);
            addressListClient.add(addressesClient);
            from_my_client.setAddresses(addressListClient);

            t_from_my_client t_from_my_client = new t_from_my_client();

//            t_from_my_client.setIdentification(null);
            t_from_my_client.setFrom_funds_code("C");
            ///////////////////////////identification//////////////
            identification identification = new identification();
            List<identification> identificationList = new ArrayList<>();
            identification.setType("NID");
            identification.setNumber("28411160225124");
            identification.setExpiry_date("2017-10-25T00:00:00");
            identification.setIssue_country("EG");
            identificationList.add(identification);
            from_my_client.setIdentification(identificationList);
//            t_from_my_client.setIdentification(identificationList);
            ///////////////////////////////////////////////////////
            from_my_client.setGender("Male");
            from_my_client.setTitle("MR");
            from_my_client.setOccupation("حاصل على دبلوم صناعى");
            from_my_client.setAddresses(addressList);
            from_my_client.setPhones(phonesList);
            String[] names = xmlUtils.getNames(trans.get(i).getNAME_AR());
            String LNAME;
            String FNAME;
            String MNAME;
            try {

                FNAME = names[0];
            } catch (Exception e) {
                FNAME = "";
            }
            try {
                MNAME = names[1];
            } catch (Exception e) {
                MNAME = "";
            }
            try {
                LNAME = names[2];
            } catch (Exception e) {
                LNAME = "";
            }
//            FNAME = names[0];
//            MNAME = names[1];
//            LNAME = names[2];
            String prefix = FNAME;
            from_my_client.setFirst_name(FNAME);
            from_my_client.setMiddle_name(MNAME);
            from_my_client.setLast_name(LNAME);

            t_from_my_clientList.add(from_my_client);
            t_from_my_client.setFrom_person(t_from_my_clientList);
            t_from_my_client.setFrom_person(t_from_my_clientList);
            t_from_my_client.setFrom_country("EG");
            t.setT_from_my_client(t_from_my_client);
            t.setT_from(null);
            t.setT_to_my_client(null);
            /////////////////////////////////////////////////////ToMyClient///////////////////////
            t_to_my_client t_to_my_client = new t_to_my_client();
            t_to_my_client.setTo_funds_code("E");

            /////////////////////////////////to_account///////////////////////////////////////////
            to_account to_account = new to_account();
            to_account.setInstitution_name("HDB");
            to_account.setSwift("SWXX11EG");
            to_account.setNon_bank_institution("fallse");
            to_account.setBranch("Zamalek");
            to_account.setAccount(trans.get(i).getACCOUNT_N());
            to_account.setCurrency_code("EGP");
            to_account.setPersonal_account_type("SV");
            to_account.setOpened("Opening date");
            to_account.setBalance(trans.get(i).getBALANCE().toString());
            to_account.setDate_balance("date balance");
            to_account.setStatus_code("Nw");
            ////////////////////t_account Signatory///////////////////
            signatory signatory = new signatory();
            List<signatory> signatoryList = new ArrayList<>();
            signatory.setIs_primary("true");
            signatory.setRole("Role");
            t_person t_person = new t_person();
            signatoryList.add(signatory);
            to_account.setSignatory(signatoryList);

            ReportingPerson tFrom = new ReportingPerson();
            t_person.setGender("ToMyClient Gender");
            t_person.setTitle("MR");
            String[] tFromNames = xmlUtils.getNames(trans.get(i).getNAME_AR());
            String tFromLNAME;
            String tFromFNAME;
            String tFromMNAME;
            try {

                tFromFNAME = tFromNames[0];
            } catch (Exception e) {
                tFromFNAME = "";
            }
            try {
                tFromMNAME = tFromNames[1];
            } catch (Exception e) {
                tFromMNAME = "";
            }
            try {
                tFromLNAME = tFromNames[2];
            } catch (Exception e) {
                tFromLNAME = "";
            }
            String tFromprefix = tFromFNAME;
            t_person.setPrefix(tFromprefix);
            t_person.setFirst_name(tFromFNAME);
            t_person.setMiddle_name(tFromMNAME);
            t_person.setLast_name(tFromLNAME);
            t_person.setSsn(user.getUsers().getNationalId());
            t_person.setBirthdate("1984-11-16T00:00:00");
            t_person.setNationality1("EG");
            t_person.setResidence("EG");

            ///////////////////////////t_from Addresses////////////////////
            Address tFromaddressClient = new Address();
            addresses tFromaddressesClient = new addresses();
            List<Address> tFromaddressClientList = new ArrayList<>();
            List<addresses> tFromaddressListClient = new ArrayList<>();
            tFromaddressClient.setAddress_type("B");
            tFromaddressClient.setAddress(user.getUsers().getAddress());
            tFromaddressClient.setCity("City");
            tFromaddressClient.setCountry_code("EG");
            tFromaddressClient.setState("Cairo");
            tFromaddressClient.setAddress("address");
            tFromaddressClientList.add(tFromaddressClient);
            tFromaddressesClient.setAddress(tFromaddressClientList);
            tFromaddressListClient.add(tFromaddressesClient);
            t_person.setAddresses(tFromaddressListClient);
            //////////////////////////////t_from phones////////////////////////
            phone tFromphoneClient = new phone();
            List<phone> tFromphoneLisClient = new ArrayList<>();
            phones tFromphonessClient = new phones();
            List<phones> tFromphonesListClient = new ArrayList<>();
            tFromphoneClient.setTph_contact_type("M");
            tFromphoneClient.setTph_communication_type("P");
            tFromphoneClient.setTph_number("01000000");
            tFromphoneLisClient.add(tFromphoneClient);
            tFromphonessClient.setPhone(tFromphoneLisClient);
            tFromphonesListClient.add(tFromphonessClient);
            t_person.setPhones(tFromphonesListClient);
            //////////////////////////////

            tFrom.setOccupation(trans.get(i).getEMPLOYMENT_STATUS_71());
            List<t_person> tFromLst = new ArrayList<>();
            tFromLst.add(t_person);
            signatory.setT_person(tFromLst);
            ////////////////////////////////t_account Signatory/////////////////
//            signatory signatory = new signatory();
//            List<signatory> signatoryList = new ArrayList<>();
//            to_account.setSignatory(signatoryList);
            t_to_my_client.setTo_account(to_account);

            t.setT_to_my_client(t_to_my_client);

            ////////////////////////////////////////////////////
            tranList.add(t);
            r.setTransaction(tranList);
//            ReportedTransactions reportedTransactions = null;
//            Set<ReportedTransactions> rSet = null;
//            System.out.println("FirstLS::    " + FirstLS.size());
//
//            reportedTransactions = new ReportedTransactions();
//
//            reportedTransactions.setFilesHis(fh);
//            reportedTransactions.setTransRef(trans.get(i).getFTRECID());
//            reportedTransactions.setTransCurrency(trans.get(i).getFTDEBIT_CURRENCY());
//            reportedTransactions.setRepFolder(xmlOutput);
//
////            reportDao.saveReportedTrans(rt);
//            if (uploadedFileName != null) {
//                File path = new File(xmlOutput + "\\" + uploadedFileName);
//                FileUtils.copyFile(new File(tempPath), path);
//            } else {
//                tempPath = null;
//                System.out.println("FileName::::   " + uploadedFileName);
//
//            }
//            if (tempPath != null) {
//                File tempFile = new File(tempPath);
//                tempFile.delete();
//            } else {
//
//            }
//
//            reList.add(reportedTransactions);
//            System.out.println("reList::::  SIze::::  " + reList.size());
//            rSet = new HashSet<>(reList);
//            fh.setReportedTransactionses(rSet);
        }

        r.setReporting_person(reportingPerson);

        fh.setId(repId);
        fh.setAction(action);
        fh.setReason(reason);
        fh.setReportType("STR");
        fh.setStatus("N");
        fh.setFolderpath(xmlOutput);
        fh.setCusnumber(FirstLS.get(0).getCUS_NUMBER());
        fh.setTransnumber(FirstLS.get(0).getFTRECID());

        fh.setInputter(user.getUserName());
        Date date = new Date();
        fh.setDateTime(new Timestamp(date.getTime()));
        fh.setFilePath(xmlOutput + "//" + repId + ".xml");

        try {
            xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
            File dir = new File(xmlOutput);
            dir.mkdir();

            JAXBContext jaxbContext = JAXBContext.newInstance(report.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(r, new File(xmlOutput + "\\" + repId + ".xml"));

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < trans.size(); i++) {
            ReportedTransactions reportedTransactions = null;
            Set<ReportedTransactions> rSet = null;
            System.out.println("FirstLS::    " + FirstLS.size());

            reportedTransactions = new ReportedTransactions();

            reportedTransactions.setFilesHis(fh);
            reportedTransactions.setTransRef(trans.get(i).getFTRECID());
            reportedTransactions.setTransCurrency(trans.get(i).getDEBIT_CURRENCY());
            reportedTransactions.setRepFolder(xmlOutput);

//            reportDao.saveReportedTrans(rt);
            if (uploadedFileName != null) {
                File path = new File(xmlOutput + "\\" + uploadedFileName);
                FileUtils.copyFile(new File(tempPath), path);
            } else {
                tempPath = null;
                System.out.println("FileName::::   " + uploadedFileName);

            }
            if (tempPath != null) {
                File tempFile = new File(tempPath);
                tempFile.delete();
            } else {

            }

            reList.add(reportedTransactions);
            System.out.println("reList::::  SIze::::  " + reList.size());
            rSet = new HashSet<>(reList);

            fh.setReportedTransactionses(rSet);
        }

        reportDao.saveFile(fh);
        FacesMessage msg = new FacesMessage("تم انشاء التقرير");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public UploadedFile getFileName() {
        return fileName;
    }

    public void setFileName(UploadedFile fileName) {
        this.fileName = fileName;
    }

    public FtFirst getSelecttedCase() {
        return selecttedCase;
    }

    public void setSelecttedCase(FtFirst selecttedCase) {
        this.selecttedCase = selecttedCase;
    }

}
