/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.ReviseFilesDao;
import com.goaml.model.FilesHis;
import com.goaml.model.UploadedFiles;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "reviseFiles")
@ViewScoped

public class ReviseFilesBean {

    private List<UploadedFiles> files = new ArrayList<UploadedFiles>();
    ReviseFilesDao dao;

    public ReviseFilesBean() {
        dao = new ReviseFilesDao();
        files = dao.getUploadedFiles();
    }

    public List<UploadedFiles> getFiles() {

        return files;
    }

    public void setFiles(List<UploadedFiles> files) {
        this.files = files;
    }

    public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        CellStyle style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

        for (Row row : sheet) {
            for (Cell cell : row) {
                cell.setCellValue(cell.getStringCellValue().toUpperCase());
                cell.setCellStyle(style);
            }
        }
    }
}
