/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.utils.DirectoryWatchDemo;
import static com.goaml.utils.DirectoryWatchDemo.watchService;
import com.goaml.utils.HandelSession;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "chartView")
@ViewScoped
public class HomePageBean implements Serializable {

    UsersBean users;

    public HomePageBean() throws InterruptedException {
//          DirectoryWatchDemo directoryWatchDemo = new DirectoryWatchDemo();
        ExecutorService service = Executors.newFixedThreadPool(4);
        service.submit(new Runnable() {
            public void run() {
                try {
                    watchService();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });
        HandelSession session = new HandelSession();
        users = (UsersBean) session.getAttribute("userBean");
//         if (users.getUsers().getRoleId().toString() != "2") {
//            RequestContext.getCurrentInstance().execute("document.getElementById('ExtractReports').style.display = 'none'");
//        } if(users.getUsers().getRoleId().toString() != "1"){
//            RequestContext.getCurrentInstance().execute("document.getElementById('Reports').style.display = 'none'");
//        }
//        if(users.getUsers().getRoleId().toString() != "1"){
//            RequestContext.getCurrentInstance().execute("document.getElementById('ExtractReports').style.display = 'none'");
//        }
//        if (users.getUsers().getRoleId().toString() != "2") {
//            RequestContext.getCurrentInstance().execute("document.getElementById('SystemReports').style.display = 'none'");
//        }
    
    }

    private PieChartModel pieModel1;
    private PieChartModel pieModel2;

    @PostConstruct
    public void init() {
        createPieModels();
    }

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public PieChartModel getPieModel2() {
        return pieModel2;
    }

    private void createPieModels() {
        createPieModel1();
        createPieModel2();
    }

    private void createPieModel1() {
        pieModel1 = new PieChartModel();

        pieModel1.set("STR", 540);
        pieModel1.set("SAR", 325);
        pieModel1.set("AIF", 702);
        pieModel1.setTitle("Reports Pie");
        pieModel1.setLegendPosition("w");

    }

    private void createPieModel2() {
        pieModel2 = new PieChartModel();

        pieModel2.set("Brand 1", 540);
        pieModel2.set("Brand 2", 325);
        pieModel2.set("Brand 3", 702);
        pieModel2.set("Brand 4", 421);

        pieModel2.setTitle("Custom Pie");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
    }

}
