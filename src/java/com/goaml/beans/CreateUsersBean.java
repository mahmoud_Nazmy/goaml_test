/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.GeneralData;
import com.goaml.dao.ModifyUsersDao;
import com.goaml.model.Users;
import com.goaml.utils.JsfUtil;
import java.math.BigDecimal;
import java.util.TreeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "CreateUsrBean")
@ViewScoped
public class CreateUsersBean {

    private String firstName;
    private String middleName;
    private String title;
    private String userName;
    private String gender;
    private String nid;
    private String phone;
    private String email;
    private String role;

    private TreeMap rolesMap;
    GeneralData gd;
    private String selectedRole;
    ModifyUsersDao dao;
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("resources/messages");

    public CreateUsersBean() {
        gd = new GeneralData();
        dao = new ModifyUsersDao();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void createUser(ActionEvent event) {

        Users u = new Users();
        u.setUsername(userName);
        u.setGender(gender);
        u.setTitle(title);
        u.setRoleId(new BigDecimal(selectedRole));
        u.setFirstName(firstName);
        u.setMidlleName(middleName);
        u.setNationalId(nid);
        u.setPhone(phone);
        u.setEmail(email);
        try {
            dao.saveUser(u);
            FacesMessage msg = new FacesMessage("تم انشاء المستخدم");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(bundle.getString("CreateUserError"));
            e.printStackTrace();
        }

    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public TreeMap getRolesMap() {
        rolesMap = gd.getRolesMap();
        return rolesMap;
    }

    public void setRolesMap(TreeMap rolesMap) {
        this.rolesMap = rolesMap;
    }

    public String getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(String selectedRole) {
        this.selectedRole = selectedRole;
    }

}
