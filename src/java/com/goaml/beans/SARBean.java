/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.CreateReportDao;
import com.goaml.dao.GeneralData;
import com.goaml.dao.SARDao;
import com.goaml.model.FilesHis;
import com.goaml.model.FtFirst;
import com.goaml.model.ReportedTransactions;
import com.goaml.model.SAR;
import com.goaml.model.SARCustomer;
import com.goaml.utils.HandelSession;
import com.goaml.utils.JsfUtil;
import com.goaml.utils.XmlUtils;
import com.goaml.xml.Address;
import com.goaml.xml.Location;
import com.goaml.xml.ReportingPerson;
import com.goaml.xml.activity;
import com.goaml.xml.addresses;
import com.goaml.xml.identification;
import com.goaml.xml.person;
import com.goaml.xml.phone;
import com.goaml.xml.phones;
import com.goaml.xml.report;
import com.goaml.xml.report_indicators;
import com.goaml.xml.report_parties;
import com.goaml.xml.report_party;
import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author CODO
 */
@ManagedBean(name = "SARBean")
@ViewScoped
public class SARBean implements Serializable {

    @Getter
    @Setter
    Boolean TABNONBANK;
    @Getter
    @Setter
    Boolean TABBANK = true;
    @Getter
    @Setter
    String first_name;
    @Getter
    @Setter
    String middle_name;
    @Getter
    @Setter
    String last_name;
    @Getter
    @Setter
    String ssn;
    @Getter
    @Setter
    String tph_number;
    @Getter
    @Setter
    String address;
    @Getter
    @Setter
    String city;
    @Getter
    @Setter
    String reason;
    @Getter
    @Setter
    String action;
    @Getter
    @Setter
    String comments;
    @Getter
    @Setter
    String Gender;
    @Getter
    @Setter
    String occupation;
    @Getter
    @Setter
    String expiry_date;
    @Getter
    @Setter
    String significance;
    @Getter
    @Setter
    String cusNumber;
    @Getter
    @Setter
    String transNumber;
    @Getter
    @Setter
    String xmlOutput;
    @Getter
    @Setter
    String nationality;
    @Getter
    @Setter
    String dateOfBirth;
    FilesHis fh;
    GeneralData gd;
    UsersBean user = null;
    CreateReportDao reportDao;
    SARDao sARDao;
    private String repId;
    String filePath = "E:\\xml\\16.xml";
    private String savPath;
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("resources/messages");
    XmlUtils xmlUtils;
    SARCustomer sARCustomer;

    public SARBean() {
        reportDao = new CreateReportDao();
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
        fh = new FilesHis();
        gd = new GeneralData();
        xmlUtils = new XmlUtils();
        sARDao = new SARDao();

    }
    private List<FtFirst> FirstLS = new ArrayList<FtFirst>();

    public List<FtFirst> getFirstLS() {
        FirstLS = reportDao.getOpenRegistr(transNumber, cusNumber);
        return FirstLS;

    }

    public void setFirstLS(List<FtFirst> FirstLS) {
        this.FirstLS = FirstLS;
    }

    public void getData(ActionEvent e) {
        if (TABBANK) {

//            fh.setId(repId);
//            fh.setCusnumber(cusNumber);
//            fh.setAction(comments);
//            fh.setReason(reason);
//            fh.setTransnumber(transNumber);
//            fh.setFilePath("E:\\xml\\" + repId + ".xml");
//            fh.setStatus("N");
//            reportDao.saveFile(fh);
//
//            reportDao.saveFile(fh);
        } else {
//            ReadObj();
        }
//        fh.setId(repId);
//        fh.setCusnumber(cusNumber);
//        fh.setAction(action);
//        fh.setReason(reason);
//        fh.setTransnumber(transNumber);
//        fh.setFilePath("E:\\xml\\" + repId + ".xml");
//        fh.setStatus("N");
//        reportDao.saveFile(fh);

    }

    public void BankCustomer(ActionEvent e) {
        List<ReportedTransactions> reList = new ArrayList<>();
        String num = "100017";
        repId = num.concat(JsfUtil.generatePIN());
        fh = new FilesHis();
        xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
        sARCustomer = sARDao.getSarCustomer(cusNumber);
        phones phoness = new phones();
        Address a = new Address();
        addresses add = new addresses();
//        Location location = new Location();
        phone phone = new phone();
        List<Address> address = new ArrayList<>();
        List<phone> phoneLis = new ArrayList<>();
        List<addresses> addressList = new ArrayList<>();
        List<phones> phonesList = new ArrayList<>();
        List<Location> locationList = new ArrayList<>();
        List<report_indicators> report_indicators = new ArrayList<>();
        //////create Report Header//////////////
        report r = xmlUtils.createRepHeader(repId, reason, action, "SAR");

        //////phoness///////
        phone.setTph_contact_type("M");
        phone.setTph_communication_type("P");
        phone.setTph_number(sARCustomer.getSMS());
        phoneLis.add(phone);
        phoness.setPhone(phoneLis);
        phonesList.add(phoness);

        ///////addressList////////
        a.setAddress_type("B");
        a.setAddress(sARCustomer.getADDRESS());
        a.setCity(sARCustomer.getCOUNTRY());
        a.setCountry_code(sARCustomer.getCOUNTRY());
        a.setState("Cairo");
        a.setAddress("address");
        address.add(a);
        add.setAddress(address);
        addressList.add(add);
        ///////////Location/////////////
        String Address_type = "B";
        String Address = sARCustomer.getSTREET();
        String city = "Cairo";
        String country_code = sARCustomer.getCOUNTRY();
        String state = "القاهرة";
        Location location = xmlUtils.createLocation(Address_type, Address, city, country_code, state);
        locationList.add(location);
        r.setLocation(locationList);

        ////////////////////reportingPerson
        ReportingPerson reportingPerson = xmlUtils.addReportingPerson(user, phonesList, addressList);
        r.setReporting_person(reportingPerson);
        /////////////////////Report Indicators

        List<String> indicators = new ArrayList<>();
        indicators.add("CC");
        indicators.add("CIUR");
        report_indicators indicator = xmlUtils.addReportIndicators(indicators);
        report_indicators.add(indicator);
        r.setReport_indicators(report_indicators);
        //////////////////////
        person person = new person();
        List<person> personList = new ArrayList<>();
        ///////////////////////////identification//////////////
        identification identification = new identification();
        List<identification> identificationList = new ArrayList<>();
        identification.setType(sARCustomer.getLEGAL_DOC_NAME());
        identification.setNumber(sARCustomer.getLEGAL_ID());
        identification.setExpiry_date(sARCustomer.getLEGAL_EXP_DATE());
        identification.setIssue_country("EG");
        identificationList.add(identification);
        person.setIdentification(identificationList);
        ////////////////////////
        
        person.setAddresses(addressList);
        person.setPhones(phonesList);
        person.setTitle(sARCustomer.getTITLE());
        String[] names = xmlUtils.getNames(sARCustomer.getNAME_AR());
        String LNAME;
        String FNAME;
        String MNAME;
        try {

            FNAME = names[0];
        } catch (Exception ee) {
            FNAME = "";
        }
        try {
            MNAME = names[1];
        } catch (Exception ee) {
            MNAME = "";
        }
        try {
            LNAME = names[2];
        } catch (Exception ee) {
            LNAME = "";
        }
        person.setFirst_name(FNAME);
        person.setMiddle_name(MNAME);
        person.setLast_name(LNAME);
        person.setGender(sARCustomer.getGENDER());
        person.setNationality1(sARCustomer.getNATIONALITY());
        person.setSsn(sARCustomer.getLEGAL_ID());
        person.setPrefix(first_name);

        personList.add(person);
        report_parties report_parties = new report_parties();
        report_party reportParty = new report_party();
        List<report_party> reportPartyList = new ArrayList<>();
        reportParty.setComments(comments);
        reportParty.setPerson(personList);
        reportParty.setReason(reason);
        reportParty.setSignificance(significance);
        reportPartyList.add(reportParty);
        activity activity = new activity();
        List<activity> activityList = new ArrayList<>();
        activity.setReport_parties(report_parties);
        report_parties.setReportParty(reportPartyList);
        activityList.add(activity);
        r.setActivity(activityList);

        fh.setId(repId);
        fh.setCusnumber(cusNumber);
        fh.setAction(action);
        fh.setReason(reason);
        fh.setComments(comments);
        fh.setStatus("N");
        fh.setFolderpath(xmlOutput);
        fh.setInputter(user.getUserName());
        Date date = new Date();
        fh.setDateTime(new Timestamp(date.getTime()));
        fh.setFilePath(xmlOutput + "//" + repId + ".xml");
        fh.setReportType("SAR");
        try {
            xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
            File dir = new File(xmlOutput);
            dir.mkdir();

            JAXBContext jaxbContext = JAXBContext.newInstance(report.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(r, new File(xmlOutput + "\\" + repId + ".xml"));

        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        ReportedTransactions reportedTransactions = null;
        Set<ReportedTransactions> rSet = null;
        System.out.println("FirstLS::    " + FirstLS.size());

        reportedTransactions = new ReportedTransactions();

        reportedTransactions.setFilesHis(fh);
        reportedTransactions.setRepFolder(xmlOutput);
//         if (uploadedFileName != null) {
//                File path = new File(xmlOutput + "\\" + uploadedFileName);
//                FileUtils.copyFile(new File(tempPath), path);
//            } else {
//                tempPath = null;
//                System.out.println("FileName::::   " + uploadedFileName);
//
//            }
//            if (tempPath != null) {
//                File tempFile = new File(tempPath);
//                tempFile.delete();
//            } else {
//
//            }

        reList.add(reportedTransactions);
        System.out.println("reList::::  SIze::::  " + reList.size());
        rSet = new HashSet<>(reList);
        fh.setReportedTransactionses(rSet);
        reportDao.saveFile(fh);
        FacesMessage msg = new FacesMessage("تم انشاء التقرير");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void nonBankSar(ActionEvent e) {
        List<ReportedTransactions> reList = new ArrayList<>();
        String num = "100017";
        repId = num.concat(JsfUtil.generatePIN());
        fh = new FilesHis();
        xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
        phones phoness = new phones();
        Address a = new Address();
        addresses add = new addresses();
//        Location location = new Location();
        phone phone = new phone();
        List<Address> address = new ArrayList<>();
        List<phone> phoneLis = new ArrayList<>();
        List<addresses> addressList = new ArrayList<>();
        List<phones> phonesList = new ArrayList<>();
        List<Location> locationList = new ArrayList<>();
        List<report_indicators> report_indicators = new ArrayList<>();
        //////create Report Header//////////////
        report r = xmlUtils.createRepHeader(repId, reason, action, "SAR");

        //////phoness///////
        phone.setTph_contact_type("M");
        phone.setTph_communication_type("P");
        phone.setTph_number(tph_number);
        phoneLis.add(phone);
        phoness.setPhone(phoneLis);
        phonesList.add(phoness);
        ///////addressList////////
        a.setAddress_type("B");
        a.setAddress(user.getUsers().getAddress());
        a.setCity("City");
        a.setCountry_code("EG");
        a.setState("Cairo");
        a.setAddress("address");
        address.add(a);
        add.setAddress(address);
        addressList.add(add);
        ///////////Location/////////////
        String Address_type = "B";
        String Address = "وسط البلد";
        String city = "Cairo";
        String country_code = "EG";
        String state = "القاهرة";
        Location location = xmlUtils.createLocation(Address_type, Address, city, country_code, state);
        locationList.add(location);
        r.setLocation(locationList);

        ////////////////////reportingPerson
        ReportingPerson reportingPerson = new ReportingPerson();
        reportingPerson.setAddresses(addressList);
        reportingPerson.setPhones(phonesList);
        reportingPerson.setGender(user.getUsers().getGender());
        reportingPerson.setTitle(user.getUsers().getTitle());
        reportingPerson.setFirst_name(user.getUsers().getFirstName());
        reportingPerson.setLast_name(user.getUsers().getLastName());
        reportingPerson.setBirthdate(user.getUsers().getBirthDate());
        reportingPerson.setSsn(user.getUsers().getNationalId());
        reportingPerson.setAddresses(addressList);
        reportingPerson.setPhones(phonesList);
        reportingPerson.setEmail(user.getUsers().getEmail());
        reportingPerson.setOccupation(user.getUsers().getOccupation());
        r.setReporting_person(reportingPerson);
        /////////////////////Report Indicators
        report_indicators indicator = new report_indicators();
        List<String> indicators = new ArrayList<>();
        indicators.add("CC");
        indicators.add("CIUR");
        indicator.setIndicator(indicators);
        report_indicators.add(indicator);
        r.setReport_indicators(report_indicators);
        //////////////////////
        person person = new person();
        List<person> personList = new ArrayList<>();
        person.setAddresses(addressList);
        person.setPhones(phonesList);
        person.setTitle("MR");
        person.setFirst_name(first_name);
        person.setMiddle_name(middle_name);
        person.setLast_name(last_name);
        person.setGender(Gender);
        person.setNationality1(nationality);
        person.setSsn(ssn);
        person.setPrefix(first_name);

        personList.add(person);
        report_parties report_parties = new report_parties();
        report_party reportParty = new report_party();
        List<report_party> reportPartyList = new ArrayList<>();
        reportParty.setComments(comments);
        reportParty.setPerson(personList);
        reportParty.setSignificance(significance);
        reportPartyList.add(reportParty);
        activity activity = new activity();
        List<activity> activityList = new ArrayList<>();
        activity.setReport_parties(report_parties);
        report_parties.setReportParty(reportPartyList);
        activityList.add(activity);
        r.setActivity(activityList);

        fh.setId(repId);
        fh.setAction(action);
        fh.setReason(reason);
        fh.setStatus("N");
        fh.setFolderpath(xmlOutput);
        fh.setInputter(user.getUserName());
        Date date = new Date();
        fh.setDateTime(new Timestamp(date.getTime()));
        fh.setFilePath(xmlOutput + "//" + repId + ".xml");
        fh.setReportType("SAR");
        try {
            xmlOutput = gd.getSubRepPath("XMLOutput") + repId;
            File dir = new File(xmlOutput);
            dir.mkdir();

            JAXBContext jaxbContext = JAXBContext.newInstance(report.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(r, new File(xmlOutput + "\\" + repId + ".xml"));

        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        ReportedTransactions reportedTransactions = null;
        Set<ReportedTransactions> rSet = null;
        System.out.println("FirstLS::    " + FirstLS.size());

        reportedTransactions = new ReportedTransactions();

        reportedTransactions.setFilesHis(fh);
        reportedTransactions.setRepFolder(xmlOutput);
//         if (uploadedFileName != null) {
//                File path = new File(xmlOutput + "\\" + uploadedFileName);
//                FileUtils.copyFile(new File(tempPath), path);
//            } else {
//                tempPath = null;
//                System.out.println("FileName::::   " + uploadedFileName);
//
//            }
//            if (tempPath != null) {
//                File tempFile = new File(tempPath);
//                tempFile.delete();
//            } else {
//
//            }

        reList.add(reportedTransactions);
        System.out.println("reList::::  SIze::::  " + reList.size());
        rSet = new HashSet<>(reList);
        fh.setReportedTransactionses(rSet);
        reportDao.saveFile(fh);
        FacesMessage msg = new FacesMessage("تم انشاء التقرير");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onTabChange(TabChangeEvent event) {
        if (event.getTab().getId().equals("TABBANK")) {
            TABBANK = true;
            TABNONBANK = false;
        } else {
            TABBANK = false;
            TABNONBANK = true;
        }

    }

    public void onTabClose(TabCloseEvent event) {

        if (event.getTab().getId().equals("TABBANK")) {
            TABBANK = false;
        } else {
            TABNONBANK = false;
        }
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getSavPath() {
        return savPath;
    }

    public void setSavPath(String savPath) {
        this.savPath = savPath;
    }

}
