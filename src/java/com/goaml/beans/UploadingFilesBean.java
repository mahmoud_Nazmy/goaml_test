/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.GeneralData;
import com.goaml.utils.HandelSession;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "FilesBean")
@ViewScoped
public class UploadingFilesBean implements Serializable {

    private UsersBean user;
    GeneralData gd;
    private TreeMap filesMap;
    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadingFilesBean() {
        HandelSession session = new HandelSession();
        user = (UsersBean) session.getAttribute("userBean");
        gd = new GeneralData();
        filesMap = gd.getFilesMap();
    }

    public void handleFileUpload(FileUploadEvent event) throws IOException {
        this.file = event.getFile();
        String line;
        String cvsSplitBy = ",";
        InputStream input = file.getInputstream();
        BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"));
        while ((line = br.readLine()) != null) {
            // use comma as separator
            try {
                String[] details = line.split(cvsSplitBy);
                String ID = details[0];
                String COMPANY = details[1];
                String BRANCH = details[2];
                String OPERATION_CORE_ID = details[3];
                String PRODUCT_ID = details[4];
                String OPERATION = details[5];
                BigDecimal OPERATION_CODE;
                try {
                    OPERATION_CODE = new BigDecimal(OPERATION);

                } catch (Exception e) {
                }
                String OPERATION_Dt = details[6];
                DateFormat df = new SimpleDateFormat("dd-MMM-yy");
                Date OPERATION_DATE= new Date();
                try {
                    OPERATION_DATE = df.parse(OPERATION_Dt);

                } catch (Exception e) {
                }
                String CURRENCY = details[7];
                String CURRENCY_AMOUNT = details[8];
                String AMOUNT = details[9];
                String MANAGER = details[10];
                String ACCOUNT_ID = details[11];
                String CUST_ID = details[12];
                String EXTERNAL_ID = details[13];
                String EXTERNAL_NAME = details[14];
                String EXTERNAL_BIC = details[15];
                String EXTERNAL_ADDRESS1 = details[16];
                String EXTERNAL_ADDRESS2 = details[17];
                String MODIFIED_BY = details[18];
                String MODIFIED_DATE = details[19];
                String EXTERNAL_COUNTRY;
                try {
                    EXTERNAL_COUNTRY = details[20];
                } catch (Exception e) {
                    EXTERNAL_COUNTRY = "";
                }
                String DEBIT_CREDIT_FLAG;
                try {
                    DEBIT_CREDIT_FLAG = details[20];
                } catch (Exception e) {
                    DEBIT_CREDIT_FLAG = "";
                }
                System.out.println(ID + '-' + COMPANY);
//                Prooperation prop = new Prooperation();
//                prop.setId(ID);
//                prop.setCompany(COMPANY);
//                prop.setBranch(BRANCH);
//                prop.setOperationCoreId(OPERATION_CORE_ID);
//                prop.setProductId(PRODUCT_ID);
//                prop.setOperationCode(OPERATION_CODE);
//                prop.setOperationDate(OPERATION_DATE);
//                prop.setCurrency(CURRENCY);
//                prop.setCurrencyAmount(CURRENCY_AMOUNT);
//                prop.setAmount(AMOUNT);
//                prop.setManager(MANAGER);
//                prop.setAccountId(ACCOUNT_ID);
//                prop.setCustId(CUST_ID);
//                prop.setExternalId(EXTERNAL_ID);
//                prop.setExternalName(EXTERNAL_NAME);
//                prop.setExternalBic(EXTERNAL_BIC);
//                prop.setExternalAddress1(EXTERNAL_ADDRESS1);
//                prop.setExternalAddress2(EXTERNAL_ADDRESS2);
//                prop.setModifiedBy(MODIFIED_BY);
//                prop.setModifiedDate(MODIFIED_DATE);
//                prop.setExternalCountry(EXTERNAL_COUNTRY);
//                prop.setDebitCreditFlag(DEBIT_CREDIT_FLAG);
//
//                UploadDataDao upload = new UploadDataDao();
//                upload.saveOperation(prop);
            } catch (Exception e) {

            }
        }
    }

    public TreeMap getFilesMap() {
        return filesMap;
    }

    public void setFilesMap(TreeMap filesMap) {
        this.filesMap = filesMap;
    }

}
