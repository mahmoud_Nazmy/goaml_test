/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.beans;

import com.goaml.dao.ReturnedRepDao;
import com.goaml.model.ReturnedRepData;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mahmoud
 */
@ManagedBean(name = "EditSar")
@ViewScoped
public class EditSarReport {

    private String paramRepId;
    ReturnedRepDao repDao;
    private ReturnedRepData repData;

    public EditSarReport() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        paramRepId = req.getParameter("repId");
        System.out.println(":::  " + paramRepId);
        repDao = new ReturnedRepDao();
        repData = repDao.getReturnedReportsData(paramRepId);

    }

    public String getParamRepId() {
        return paramRepId;
    }

    public void setParamRepId(String paramRepId) {
        this.paramRepId = paramRepId;
    }

    public ReturnedRepData getRepData() {
        return repData;
    }

    public void setRepData(ReturnedRepData repData) {
        this.repData = repData;
    }

}
