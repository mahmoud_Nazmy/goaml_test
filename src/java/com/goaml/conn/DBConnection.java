/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.conn;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Mahmoud
 */
public class DBConnection implements Serializable{
   java.sql.Connection conn;
    public java.sql.Connection con;
    String url = "jdbc:oracle:thin:@localhost:1521:goaml";
    String user = "t24build";
    String pass = "t24build";

    public void startConn() throws ClassNotFoundException, SQLException {
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            con = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void closeConn() throws SQLException {
        con.close();
    }


}
