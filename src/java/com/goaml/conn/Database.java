/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.conn;

/**
 *
 * @author Mahmoud
 */
import java.sql.Connection;
import java.sql.DriverManager;

public class Database {

//    static String url = "jdbc:oracle:thin:@localhost:1521:oracl2k";
    static String url = "jdbc:oracle:thin:@localhost:1521:HDB";
    static String user = "goaml";
    static String pass = "goaml";

    public static Connection getConnection() {
        try {
          Class.forName("oracle.jdbc.OracleDriver");
            Connection con =DriverManager.getConnection(url, user, pass);
            return con;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

    public static void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
}
