/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.encryptor;

/**
 *
 * @author Mahmoud
 */
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;

public class DesEncryp_Decrypt {

    private static final String UNICODE_FORMAT = "UTF-8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;

    public DesEncryp_Decrypt() throws Exception {
        myEncryptionKey = "MIIEogIBAAKCAQEAtHn7swBWTSEoSa/W4Lb4g4ep1qRK2Tabu51Bs4+XUYzM0Nvi\n"
                + "sEnGREK59SfbbhUS+7tdLX+mjg/eWymp+JN7Ezqo/+bJkNifxJfXRvthMg8V+BdC\n"
                + "YeOn++EivlMDVf/qG/N7tUpbgLmEX8Vi77Q/tnvyDNhSBr2872uuWRRPdd1XsOj/\n"
                + "JEAvW5apP9iRyHJwIcC5cfN8Rcqw7ekEyKZx5yR+gekzKsEoGBTKoXjwQAxX1+jY\n"
                + "zflekVZKBhLmYfqOh/RuBNTNaY+eqGPkOrUOwvnxM9veuVL3sVIiafqugWY5tN/Y\n"
                + "uMYoTwbNNZqHDRBybVZnpkrgKUsmYGF+tWzrAQIDAQABAoIBAEW7ZB8zoCTJDPH7\n"
                + "uQC2casQffTrIehvixigmEQy1Pqzdme0rhADUVIKniM1NTmBdwZAzK3JxqbZRsbe\n"
                + "gEgFY+Z4IdhOac9Hg+PH72wDsPhQj+J9yi5ccgmGYZA8LuvJp8EJAOF5S4WfKKCA\n"
                + "ecu4T23I0itYYcktApVXe9P7XwBg0F57anPNNeOYDROlZyF1jo+8MenztByV9fAT\n"
                + "oR1LDvhJ8360Qp5LtkpJifa4+77VruvWsr5rl1mIGRW2obcuVyah7X5Ff7dkjlhv\n"
                + "hchaFFfTi8A6AO2olozJweHXP9oq6J2MBQmsvSjUWYtLOssZhcZi4dz7sD9btxSA\n"
                + "yyKRyikCgYEA8GCaU5XPzXu/SeGItjHVTtdO69CqyIEJ0Fa0EbHKdTWABQSEPVwq\n"
                + "HusvK9HVxM72EvGewNig+yr7+XE/2gwPzOJ40UyUeJ/3GcwK27F8bgswQ49xx16X\n"
                + "FCGwgFxGvTyW9410edT7/0xVYzJdJb+GWrDJiwvhK3ZSwWshujBTp9cCgYEAwDTA\n"
                + "DVNxEqqLCXAzkA88ny9c5lY7mWtnBCgQcPJhp/CoVrSd0xBr6/pzauzN/XGt25Sl\n"
                + "inNUfPyHdXa4Z5F2jRsripnZ61Asq6HAt5P/56YtfILP3WiW2DQ3ML9Cfli9CFyh\n"
                + "g0SVQeKMgnYzT1MJNE7Fg7L2yRw716lTbLukSOcCgYBYstzfkP1gDpS0d3OR/2t9\n"
                + "rzdhDtumJu9vID9L9DYH8BjwQQ1m8vZb+F6spb5i5BerP9tbv9QPOnpGPkLYe/Ds\n"
                + "onkAimiS5jwxGbi7/o8ysBA8LddHQSEwfYn6o5I6y/vNFzZuqmfeedVcSD5U6opy\n"
                + "Np/4HgcVmZDJ7HVClgiYewKBgGtBRGZrSK7M4jdLGErsZbzCtkmGl01+hIbqQHp2\n"
                + "lw4bGiUANw3fnQmawatKJ4ylUgo1Qccnol74oDJpJHAO2SPWpI58WgGYxMSlx5/p\n"
                + "g6Tls35GlO4Nfhd5085s/BL7O8kdv7c7nzqbzCLFCM7cHEfHKSrbxCDnk3E9+OK1\n"
                + "NvDDAoGAQvEDlNsD6zoy78r/k4HAE2BGRSnbaLLBINgnPvvOoJvvrCCPvPqgaryo\n"
                + "q3q6dvlE9H3c6ldp4YvqC2EZepb6LJiGexXbXUAWgsyLRCcXKWuDUgxV7R2Q6Sxt\n"
                + "Z+pdzrNnN5HHndrG+odul4UkF68S6o8XHTS3k78+8WShb2f7N50=";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }

    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }
        public String decrypt(String encryptedString) {
        String decryptedText = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString.getBytes("UTF-8"));
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText = new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }
}
