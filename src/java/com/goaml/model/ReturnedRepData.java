/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

import java.util.Date;

/**
 *
 * @author Mahmoud
 */
public class ReturnedRepData {

    private String ID;
    private String FILE_PATH;
    private String TRANSNUMBER;
    private String CUSNUMBER;
    private String REASON;
    private String ACTION;
    private String STATUS;
    private String INPUTTER;
    private Date DATE_TIME;
    private String FOLDERPATH;
    private String REPORT_TYPE;
    private String COMMENTS;
    private String TRANS_AMOUT;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFILE_PATH() {
        return FILE_PATH;
    }

    public void setFILE_PATH(String FILE_PATH) {
        this.FILE_PATH = FILE_PATH;
    }

    public String getTRANSNUMBER() {
        return TRANSNUMBER;
    }

    public void setTRANSNUMBER(String TRANSNUMBER) {
        this.TRANSNUMBER = TRANSNUMBER;
    }

    public String getCUSNUMBER() {
        return CUSNUMBER;
    }

    public void setCUSNUMBER(String CUSNUMBER) {
        this.CUSNUMBER = CUSNUMBER;
    }

    public String getREASON() {
        return REASON;
    }

    public void setREASON(String REASON) {
        this.REASON = REASON;
    }

    public String getACTION() {
        return ACTION;
    }

    public void setACTION(String ACTION) {
        this.ACTION = ACTION;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getINPUTTER() {
        return INPUTTER;
    }

    public void setINPUTTER(String INPUTTER) {
        this.INPUTTER = INPUTTER;
    }

    public Date getDATE_TIME() {
        return DATE_TIME;
    }

    public void setDATE_TIME(Date DATE_TIME) {
        this.DATE_TIME = DATE_TIME;
    }

    public String getFOLDERPATH() {
        return FOLDERPATH;
    }

    public void setFOLDERPATH(String FOLDERPATH) {
        this.FOLDERPATH = FOLDERPATH;
    }

    public String getREPORT_TYPE() {
        return REPORT_TYPE;
    }

    public void setREPORT_TYPE(String REPORT_TYPE) {
        this.REPORT_TYPE = REPORT_TYPE;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String COMMENTS) {
        this.COMMENTS = COMMENTS;
    }

    public String getTRANS_AMOUT() {
        return TRANS_AMOUT;
    }

    public void setTRANS_AMOUT(String TRANS_AMOUT) {
        this.TRANS_AMOUT = TRANS_AMOUT;
    }

}
