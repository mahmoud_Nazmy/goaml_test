/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

import java.io.Serializable;

/**
 *
 * @author Mahmoud
 */
public class ReturnedRepView {


    private String REPORT_ID;
    private String RETURN_FROM;
    private String REASON;
    private String RETURNED_TO;
    private Serializable DATE_TIME;
    private String REPORT_TYPE;
    private String REPREASON;
    private String CUSNUMBER;
    private String FILE_PATH;



    public String getREPORT_ID() {
        return REPORT_ID;
    }

    public void setREPORT_ID(String REPORT_ID) {
        this.REPORT_ID = REPORT_ID;
    }

    public String getRETURN_FROM() {
        return RETURN_FROM;
    }

    public void setRETURN_FROM(String RETURN_FROM) {
        this.RETURN_FROM = RETURN_FROM;
    }

    public String getREASON() {
        return REASON;
    }

    public void setREASON(String REASON) {
        this.REASON = REASON;
    }

    public String getRETURNED_TO() {
        return RETURNED_TO;
    }

    public void setRETURNED_TO(String RETURNED_TO) {
        this.RETURNED_TO = RETURNED_TO;
    }

    public Serializable getDATE_TIME() {
        return DATE_TIME;
    }

    public void setDATE_TIME(Serializable DATE_TIME) {
        this.DATE_TIME = DATE_TIME;
    }

    public String getREPORT_TYPE() {
        return REPORT_TYPE;
    }

    public void setREPORT_TYPE(String REPORT_TYPE) {
        this.REPORT_TYPE = REPORT_TYPE;
    }

    public String getREPREASON() {
        return REPREASON;
    }

    public void setREPREASON(String REPREASON) {
        this.REPREASON = REPREASON;
    }

    public String getCUSNUMBER() {
        return CUSNUMBER;
    }

    public void setCUSNUMBER(String CUSNUMBER) {
        this.CUSNUMBER = CUSNUMBER;
    }

    public String getFILE_PATH() {
        return FILE_PATH;
    }

    public void setFILE_PATH(String FILE_PATH) {
        this.FILE_PATH = FILE_PATH;
    }
    
    
}
