/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

/**
 *
 * @author Mahmoud
 */
public class TellerTrans {

    private String AMOUNT_LOCAL_1;
    private String AMOUNT_LOCAL_2;
    private String AMOUNT_FCY_1;
    private String CURRENCY_1;
    private String CURRENCY_2;
    private String DEPOSITER_ID;
    private String DEPOSITER_NAME;
    private String AUTH_DATE;
    private String NARRATIVE_1;
    private String TELLER_ID_1;
    private String INPUTTER;
    private String AUTHORISER;
    private String TELLER_BRANCH;
    private String TRANSACTION_TYPE;
    private String ACCOUNTNO;
    private String ACCOUNT_NAME;
    private String CURRENCY;
    private String BALANCE;
    private String TRAN_LAST_DR_CUST;
    private String TRAN_LAST_CR_CUST;
    private String DATE_LAST_DR_CUST;
    private String DATE_LAST_CR_CUST;
    private String CATEGORY;
    private String STATUS;
    private String SIGNATORY;
    private String OPENING_DATE;
    private String ACCOUNT_BRANCH;
    private String CUSTOMERNO;
    private String GENDER;
    private String TITLE;
    private String NAME_AR;
    private String NAME_EN;
    private String FAMILY_NAME;
    private String FATHER_NAME;
    private String LEGAL_ID;
    private String LEGAL_ISS_DATE;
    private String LEGAL_EXP_DATE;
    private String LEGAL_ID_DOC_NAME;
    private String LEGAL_DOC_NAME;
    private String NATIONALITY;
    private String DATE_OF_BIRTH;
    private String ADDRESS;
    private String ADDR_LOCATION;
    private String COUNTRY;
    private String STREET;
    private String JOB_TITLE;
    private String EMPLOYMENT_STATUS_71;
    private String EDUCATION_LEVEL;
    private String SECTOR;
    private String CUS_NAME;
    private String JOB_TITLE_1;
    private String SMS;
    private String TRANS_REF;

    public String getAMOUNT_LOCAL_1() {
        return AMOUNT_LOCAL_1;
    }

    public void setAMOUNT_LOCAL_1(String AMOUNT_LOCAL_1) {
        this.AMOUNT_LOCAL_1 = AMOUNT_LOCAL_1;
    }

    public String getAMOUNT_LOCAL_2() {
        return AMOUNT_LOCAL_2;
    }

    public void setAMOUNT_LOCAL_2(String AMOUNT_LOCAL_2) {
        this.AMOUNT_LOCAL_2 = AMOUNT_LOCAL_2;
    }

    public String getAMOUNT_FCY_1() {
        return AMOUNT_FCY_1;
    }

    public void setAMOUNT_FCY_1(String AMOUNT_FCY_1) {
        this.AMOUNT_FCY_1 = AMOUNT_FCY_1;
    }

    public String getCURRENCY_1() {
        return CURRENCY_1;
    }

    public void setCURRENCY_1(String CURRENCY_1) {
        this.CURRENCY_1 = CURRENCY_1;
    }

    public String getCURRENCY_2() {
        return CURRENCY_2;
    }

    public void setCURRENCY_2(String CURRENCY_2) {
        this.CURRENCY_2 = CURRENCY_2;
    }

    public String getDEPOSITER_ID() {
        return DEPOSITER_ID;
    }

    public void setDEPOSITER_ID(String DEPOSITER_ID) {
        this.DEPOSITER_ID = DEPOSITER_ID;
    }

    public String getDEPOSITER_NAME() {
        return DEPOSITER_NAME;
    }

    public void setDEPOSITER_NAME(String DEPOSITER_NAME) {
        this.DEPOSITER_NAME = DEPOSITER_NAME;
    }

    public String getTELLER_ID_1() {
        return TELLER_ID_1;
    }

    public void setTELLER_ID_1(String TELLER_ID_1) {
        this.TELLER_ID_1 = TELLER_ID_1;
    }

    public String getINPUTTER() {
        return INPUTTER;
    }

    public void setINPUTTER(String INPUTTER) {
        this.INPUTTER = INPUTTER;
    }

    public String getAUTHORISER() {
        return AUTHORISER;
    }

    public void setAUTHORISER(String AUTHORISER) {
        this.AUTHORISER = AUTHORISER;
    }

    public String getTELLER_BRANCH() {
        return TELLER_BRANCH;
    }

    public void setTELLER_BRANCH(String TELLER_BRANCH) {
        this.TELLER_BRANCH = TELLER_BRANCH;
    }

    public String getTRANSACTION_TYPE() {
        return TRANSACTION_TYPE;
    }

    public void setTRANSACTION_TYPE(String TRANSACTION_TYPE) {
        this.TRANSACTION_TYPE = TRANSACTION_TYPE;
    }

    public String getACCOUNTNO() {
        return ACCOUNTNO;
    }

    public void setACCOUNTNO(String ACCOUNTNO) {
        this.ACCOUNTNO = ACCOUNTNO;
    }

    public String getACCOUNT_NAME() {
        return ACCOUNT_NAME;
    }

    public void setACCOUNT_NAME(String ACCOUNT_NAME) {
        this.ACCOUNT_NAME = ACCOUNT_NAME;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    public String getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(String BALANCE) {
        this.BALANCE = BALANCE;
    }

    public String getTRAN_LAST_DR_CUST() {
        return TRAN_LAST_DR_CUST;
    }

    public void setTRAN_LAST_DR_CUST(String TRAN_LAST_DR_CUST) {
        this.TRAN_LAST_DR_CUST = TRAN_LAST_DR_CUST;
    }

    public String getTRAN_LAST_CR_CUST() {
        return TRAN_LAST_CR_CUST;
    }

    public void setTRAN_LAST_CR_CUST(String TRAN_LAST_CR_CUST) {
        this.TRAN_LAST_CR_CUST = TRAN_LAST_CR_CUST;
    }

    public String getDATE_LAST_DR_CUST() {
        return DATE_LAST_DR_CUST;
    }

    public void setDATE_LAST_DR_CUST(String DATE_LAST_DR_CUST) {
        this.DATE_LAST_DR_CUST = DATE_LAST_DR_CUST;
    }

    public String getDATE_LAST_CR_CUST() {
        return DATE_LAST_CR_CUST;
    }

    public void setDATE_LAST_CR_CUST(String DATE_LAST_CR_CUST) {
        this.DATE_LAST_CR_CUST = DATE_LAST_CR_CUST;
    }

    public String getCATEGORY() {
        return CATEGORY;
    }

    public void setCATEGORY(String CATEGORY) {
        this.CATEGORY = CATEGORY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSIGNATORY() {
        return SIGNATORY;
    }

    public void setSIGNATORY(String SIGNATORY) {
        this.SIGNATORY = SIGNATORY;
    }

    public String getOPENING_DATE() {
        return OPENING_DATE;
    }

    public void setOPENING_DATE(String OPENING_DATE) {
        this.OPENING_DATE = OPENING_DATE;
    }

    public String getACCOUNT_BRANCH() {
        return ACCOUNT_BRANCH;
    }

    public void setACCOUNT_BRANCH(String ACCOUNT_BRANCH) {
        this.ACCOUNT_BRANCH = ACCOUNT_BRANCH;
    }

    public String getCUSTOMERNO() {
        return CUSTOMERNO;
    }

    public void setCUSTOMERNO(String CUSTOMERNO) {
        this.CUSTOMERNO = CUSTOMERNO;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getNAME_AR() {
        return NAME_AR;
    }

    public void setNAME_AR(String NAME_AR) {
        this.NAME_AR = NAME_AR;
    }

    public String getNAME_EN() {
        return NAME_EN;
    }

    public void setNAME_EN(String NAME_EN) {
        this.NAME_EN = NAME_EN;
    }

    public String getFAMILY_NAME() {
        return FAMILY_NAME;
    }

    public void setFAMILY_NAME(String FAMILY_NAME) {
        this.FAMILY_NAME = FAMILY_NAME;
    }

    public String getFATHER_NAME() {
        return FATHER_NAME;
    }

    public void setFATHER_NAME(String FATHER_NAME) {
        this.FATHER_NAME = FATHER_NAME;
    }

    public String getLEGAL_ID() {
        return LEGAL_ID;
    }

    public void setLEGAL_ID(String LEGAL_ID) {
        this.LEGAL_ID = LEGAL_ID;
    }

    public String getLEGAL_ISS_DATE() {
        return LEGAL_ISS_DATE;
    }

    public void setLEGAL_ISS_DATE(String LEGAL_ISS_DATE) {
        this.LEGAL_ISS_DATE = LEGAL_ISS_DATE;
    }

    public String getLEGAL_EXP_DATE() {
        return LEGAL_EXP_DATE;
    }

    public void setLEGAL_EXP_DATE(String LEGAL_EXP_DATE) {
        this.LEGAL_EXP_DATE = LEGAL_EXP_DATE;
    }

    public String getLEGAL_ID_DOC_NAME() {
        return LEGAL_ID_DOC_NAME;
    }

    public void setLEGAL_ID_DOC_NAME(String LEGAL_ID_DOC_NAME) {
        this.LEGAL_ID_DOC_NAME = LEGAL_ID_DOC_NAME;
    }

    public String getLEGAL_DOC_NAME() {
        return LEGAL_DOC_NAME;
    }

    public void setLEGAL_DOC_NAME(String LEGAL_DOC_NAME) {
        this.LEGAL_DOC_NAME = LEGAL_DOC_NAME;
    }

    public String getNATIONALITY() {
        return NATIONALITY;
    }

    public void setNATIONALITY(String NATIONALITY) {
        this.NATIONALITY = NATIONALITY;
    }

    public String getDATE_OF_BIRTH() {
        return DATE_OF_BIRTH;
    }

    public void setDATE_OF_BIRTH(String DATE_OF_BIRTH) {
        this.DATE_OF_BIRTH = DATE_OF_BIRTH;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getADDR_LOCATION() {
        return ADDR_LOCATION;
    }

    public void setADDR_LOCATION(String ADDR_LOCATION) {
        this.ADDR_LOCATION = ADDR_LOCATION;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getSTREET() {
        return STREET;
    }

    public void setSTREET(String STREET) {
        this.STREET = STREET;
    }

    public String getJOB_TITLE() {
        return JOB_TITLE;
    }

    public void setJOB_TITLE(String JOB_TITLE) {
        this.JOB_TITLE = JOB_TITLE;
    }

    public String getEMPLOYMENT_STATUS_71() {
        return EMPLOYMENT_STATUS_71;
    }

    public void setEMPLOYMENT_STATUS_71(String EMPLOYMENT_STATUS_71) {
        this.EMPLOYMENT_STATUS_71 = EMPLOYMENT_STATUS_71;
    }

    public String getEDUCATION_LEVEL() {
        return EDUCATION_LEVEL;
    }

    public void setEDUCATION_LEVEL(String EDUCATION_LEVEL) {
        this.EDUCATION_LEVEL = EDUCATION_LEVEL;
    }

    public String getSECTOR() {
        return SECTOR;
    }

    public void setSECTOR(String SECTOR) {
        this.SECTOR = SECTOR;
    }

    public String getCUS_NAME() {
        return CUS_NAME;
    }

    public void setCUS_NAME(String CUS_NAME) {
        this.CUS_NAME = CUS_NAME;
    }

    public String getJOB_TITLE_1() {
        return JOB_TITLE_1;
    }

    public void setJOB_TITLE_1(String JOB_TITLE_1) {
        this.JOB_TITLE_1 = JOB_TITLE_1;
    }

    public String getSMS() {
        return SMS;
    }

    public void setSMS(String SMS) {
        this.SMS = SMS;
    }

    public String getTRANS_REF() {
        return TRANS_REF;
    }

    public void setTRANS_REF(String TRANS_REF) {
        this.TRANS_REF = TRANS_REF;
    }

    public String getAUTH_DATE() {
        return AUTH_DATE;
    }

    public void setAUTH_DATE(String AUTH_DATE) {
        this.AUTH_DATE = AUTH_DATE;
    }

    public String getNARRATIVE_1() {
        return NARRATIVE_1;
    }

    public void setNARRATIVE_1(String NARRATIVE_1) {
        this.NARRATIVE_1 = NARRATIVE_1;
    }
    
    
}
