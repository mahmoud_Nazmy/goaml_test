/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author CODO
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SAR {

@Getter @Setter String first_name  ;
@Getter @Setter String middle_name ;
@Getter @Setter String  last_name  ;
@Getter @Setter String ssn  ;
@Getter @Setter String tph_number ;
@Getter @Setter String address ;
@Getter @Setter String city  ;
@Getter @Setter String reason  ;
@Getter @Setter String comments  ;
@Getter @Setter String Gender  ;
@Getter @Setter String occupation ;
@Getter @Setter String expiry_date  ;
@Getter @Setter String significance  ;   
}
