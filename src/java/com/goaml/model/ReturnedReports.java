package com.goaml.model;
// Generated May 17, 2017 11:08:12 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * ReturnedReports generated by hbm2java
 */
public class ReturnedReports  implements java.io.Serializable {


     private int id;
     private String reportId;
     private String returnFrom;
     private String reason;
     private String returnedTo;
     private Date dateTime;

    public ReturnedReports() {
    }

    public ReturnedReports(String reportId, String returnFrom, String reason, String returnedTo, Date dateTime) {
       this.reportId = reportId;
       this.returnFrom = returnFrom;
       this.reason = reason;
       this.returnedTo = returnedTo;
       this.dateTime = dateTime;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getReportId() {
        return this.reportId;
    }
    
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }
    public String getReturnFrom() {
        return this.returnFrom;
    }
    
    public void setReturnFrom(String returnFrom) {
        this.returnFrom = returnFrom;
    }
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getReturnedTo() {
        return this.returnedTo;
    }
    
    public void setReturnedTo(String returnedTo) {
        this.returnedTo = returnedTo;
    }
    public Date getDateTime() {
        return this.dateTime;
    }
    
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }




}


