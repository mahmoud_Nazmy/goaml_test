/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author CODO
 */

@ToString
public class FtFirst {
    
@Getter @Setter	String EDUCATION_LEVEL     ;
@Getter @Setter	String SECTOR              ;
@Getter @Setter	String SHORT_NAME          ;
@Getter @Setter	String JOB_TITLE_1         ;
@Getter @Setter	String SMS                 ;
@Getter @Setter	String FTRECID             ;
@Getter @Setter	String RECID               ;
@Getter @Setter	String TRANSACTION_TYPE    ;
@Getter @Setter	String DEBIT_ACCT_NO       ;
@Getter @Setter	String DEBIT_CURRENCY      ;
@Getter @Setter	BigDecimal DEBIT_AMOUNT        ;
@Getter @Setter	String DEBIT_VALUE_DATE    ;
@Getter @Setter	String DEBIT_THEIR_REF     ;
@Getter @Setter	String CREDIT_ACCT_NO      ;
@Getter @Setter	String CREDIT_CURRENCY     ;
@Getter @Setter	BigDecimal CREDIT_AMOUNT       ;
@Getter @Setter	String CREDIT_VALUE_DATE   ;
@Getter @Setter	String PROCESSING_DATE     ;
@Getter @Setter	String DESCRRIPTION        ;
@Getter @Setter	String INPUTTER            ;
@Getter @Setter	String AUTHORISER          ;
@Getter @Setter	String FT_BRANCH           ;
@Getter @Setter	String ACCOUNT_N           ;
@Getter @Setter	String CUSTOMER_NO         ;
@Getter @Setter	String SHORT_TITLE         ;
@Getter @Setter	String CURRENCY            ;
@Getter @Setter	BigDecimal BALANCE             ;
@Getter @Setter	String TRAN_LAST_DR_CUST   ;
@Getter @Setter	String TRAN_LAST_CR_CUST   ;
@Getter @Setter	String DATE_LAST_DR_CUST   ;
@Getter @Setter	String DATE_LAST_CR_CUST   ;
@Getter @Setter	String CATEGORY            ;
@Getter @Setter	String STATUS              ;
@Getter @Setter	String SIGNATORY           ;
@Getter @Setter	String OPENING_DATE        ;
@Getter @Setter	String ACCOUNT_BRANCH      ;
@Getter @Setter	String CUS_NUMBER          ;
@Getter @Setter	String GENDER              ;
@Getter @Setter	String TITLE               ;
@Getter @Setter	String NAME_AR             ;
@Getter @Setter	String NAME_EN             ;
@Getter @Setter	String FAMILY_NAME         ;
@Getter @Setter	String FATHER_NAME         ;
@Getter @Setter	String LEGAL_ID            ;
@Getter @Setter	String LEGAL_ISS_DATE      ;
@Getter @Setter	String LEGAL_EXP_DATE      ;
@Getter @Setter	String LEGAL_ID_DOC_NAME   ;
@Getter @Setter	String LEGAL_DOC_NAME      ;
@Getter @Setter	String NATIONALITY         ;
@Getter @Setter	String DATE_OF_BIRTH       ;
@Getter @Setter	String ADDRESS             ;
@Getter @Setter	String ADDR_LOCATION       ;
@Getter @Setter	String COUNTRY             ;
@Getter @Setter	String STREET              ;
@Getter @Setter	String JOB_TITLE           ;
@Getter @Setter	String EMPLOYMENT_STATUS_71;
}
