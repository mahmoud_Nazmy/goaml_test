/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.model;

/**
 *
 * @author Mahmoud
 */
public class SARCustomer {
    private String RECID;
    private String GENDER;
    private String TITLE;
    private String NAME_AR;
    private String NAME_EN;
    private String FAMILY_NAME;
    private String FATHER_NAME;
    private String LEGAL_ID;
    private String LEGAL_ISS_DATE;
    private String LEGAL_EXP_DATE;
    private String LEGAL_ID_DOC_NAME;
    private String LEGAL_DOC_NAME;
    private String NATIONALITY;
    private String DATE_OF_BIRTH;
    private String ADDRESS;
    private String ADDR_LOCATION;
    private String COUNTRY;
    private String STREET;
    private String JOB_TITLE;
    private String EMPLOYMENT_STATUS_71;
    private String EDUCATION_LEVEL;
    private String SECTOR;
    private String SECTOR_1;
    private String SHORT_NAME;
    private String JOB_TITLE_1;
    private String SMS;
    private String TITLE_NAME;

    public String getRECID() {
        return RECID;
    }

    public void setRECID(String RECID) {
        this.RECID = RECID;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getNAME_AR() {
        return NAME_AR;
    }

    public void setNAME_AR(String NAME_AR) {
        this.NAME_AR = NAME_AR;
    }

    public String getNAME_EN() {
        return NAME_EN;
    }

    public void setNAME_EN(String NAME_EN) {
        this.NAME_EN = NAME_EN;
    }

    public String getFAMILY_NAME() {
        return FAMILY_NAME;
    }

    public void setFAMILY_NAME(String FAMILY_NAME) {
        this.FAMILY_NAME = FAMILY_NAME;
    }

    public String getFATHER_NAME() {
        return FATHER_NAME;
    }

    public void setFATHER_NAME(String FATHER_NAME) {
        this.FATHER_NAME = FATHER_NAME;
    }

    public String getLEGAL_ID() {
        return LEGAL_ID;
    }

    public void setLEGAL_ID(String LEGAL_ID) {
        this.LEGAL_ID = LEGAL_ID;
    }

    public String getLEGAL_ISS_DATE() {
        return LEGAL_ISS_DATE;
    }

    public void setLEGAL_ISS_DATE(String LEGAL_ISS_DATE) {
        this.LEGAL_ISS_DATE = LEGAL_ISS_DATE;
    }

    public String getLEGAL_EXP_DATE() {
        return LEGAL_EXP_DATE;
    }

    public void setLEGAL_EXP_DATE(String LEGAL_EXP_DATE) {
        this.LEGAL_EXP_DATE = LEGAL_EXP_DATE;
    }

    public String getLEGAL_ID_DOC_NAME() {
        return LEGAL_ID_DOC_NAME;
    }

    public void setLEGAL_ID_DOC_NAME(String LEGAL_ID_DOC_NAME) {
        this.LEGAL_ID_DOC_NAME = LEGAL_ID_DOC_NAME;
    }

    public String getLEGAL_DOC_NAME() {
        return LEGAL_DOC_NAME;
    }

    public void setLEGAL_DOC_NAME(String LEGAL_DOC_NAME) {
        this.LEGAL_DOC_NAME = LEGAL_DOC_NAME;
    }

    public String getNATIONALITY() {
        return NATIONALITY;
    }

    public void setNATIONALITY(String NATIONALITY) {
        this.NATIONALITY = NATIONALITY;
    }

    public String getDATE_OF_BIRTH() {
        return DATE_OF_BIRTH;
    }

    public void setDATE_OF_BIRTH(String DATE_OF_BIRTH) {
        this.DATE_OF_BIRTH = DATE_OF_BIRTH;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getADDR_LOCATION() {
        return ADDR_LOCATION;
    }

    public void setADDR_LOCATION(String ADDR_LOCATION) {
        this.ADDR_LOCATION = ADDR_LOCATION;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getSTREET() {
        return STREET;
    }

    public void setSTREET(String STREET) {
        this.STREET = STREET;
    }

    public String getJOB_TITLE() {
        return JOB_TITLE;
    }

    public void setJOB_TITLE(String JOB_TITLE) {
        this.JOB_TITLE = JOB_TITLE;
    }

    public String getEMPLOYMENT_STATUS_71() {
        return EMPLOYMENT_STATUS_71;
    }

    public void setEMPLOYMENT_STATUS_71(String EMPLOYMENT_STATUS_71) {
        this.EMPLOYMENT_STATUS_71 = EMPLOYMENT_STATUS_71;
    }

    public String getEDUCATION_LEVEL() {
        return EDUCATION_LEVEL;
    }

    public void setEDUCATION_LEVEL(String EDUCATION_LEVEL) {
        this.EDUCATION_LEVEL = EDUCATION_LEVEL;
    }

    public String getSECTOR() {
        return SECTOR;
    }

    public void setSECTOR(String SECTOR) {
        this.SECTOR = SECTOR;
    }

    public String getSECTOR_1() {
        return SECTOR_1;
    }

    public void setSECTOR_1(String SECTOR_1) {
        this.SECTOR_1 = SECTOR_1;
    }

    public String getSHORT_NAME() {
        return SHORT_NAME;
    }

    public void setSHORT_NAME(String SHORT_NAME) {
        this.SHORT_NAME = SHORT_NAME;
    }

    public String getJOB_TITLE_1() {
        return JOB_TITLE_1;
    }

    public void setJOB_TITLE_1(String JOB_TITLE_1) {
        this.JOB_TITLE_1 = JOB_TITLE_1;
    }

    public String getTITLE_NAME() {
        return TITLE_NAME;
    }

    public void setTITLE_NAME(String TITLE_NAME) {
        this.TITLE_NAME = TITLE_NAME;
    }

    public String getSMS() {
        return SMS;
    }

    public void setSMS(String SMS) {
        this.SMS = SMS;
    }
    
    
}
