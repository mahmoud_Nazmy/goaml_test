package com.goaml.logger;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log {

    // Root logger

  private static Logger logger;

    public static void initialize(String logFilePath) {
        // Read logging configuration
        Properties properties = new Properties();
        InputStream is = Log.class.getResourceAsStream("log4j.properties");
        try {
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            try {
                is.close();
            } catch (Exception e) {
            }
        }

        // Override log file path
        if (logFilePath != null) {
            properties.setProperty("log4j.appender.file.File", logFilePath);
        }

        // Configure logger
        PropertyConfigurator.configure(properties);

        // Set logger
        logger = Logger.getLogger("MOH");
    }

    public static void trace(String message) {
        if (logger == null) {
            System.out.println(message);
        } else {
            logger.trace(message);
        }
    }

    public static void debug(String message) {
        if (logger == null) {
            System.out.println(message);
        } else {
            logger.debug(message);
        }
    }

    public static void info(String message) {
        if (logger == null) {
            System.out.println(message);
        } else {
            logger.info(message);
        }
    }

    public static void warn(String message) {
        if (logger == null) {
            System.err.println(message);
        } else {
            logger.warn(message);
        }
    }

    public static void error(String message) {
        if (logger == null) {
            System.err.println(message);
        } else {
            logger.error(message);
        }
    }

    public static void fatal(String message) {
        if (logger == null) {
            System.err.println(message);
        } else {
            logger.fatal(message);
        }
    }

    public static void printStackTrace(Throwable e) {
        // Prepare string buffer
        StringWriter writer = new StringWriter();

        // Write stack trace to buffer
        e.printStackTrace(new PrintWriter(writer));

        // Print buffer content to log
        logger.error(writer.toString());
    }

    public static void beginMethod() {
        debug("Starting: " + getCurrentMethod());
    }

    public static void endMethod() {
        debug("Exiting: " + getCurrentMethod());
    }

    private static String getCurrentMethod() {
        // Get stack trace
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

        // Get class and method name
        int level = 3;
        String className = stackTrace[level].getClassName();
        String methodName = stackTrace[level].getMethodName();

        // Remove package name
        int dotIndex = className.lastIndexOf('.');
        if (dotIndex >= 0) {
            className = className.substring(dotIndex + 1);
        }

        // Return full method name
        return className + "." + methodName + "()";
    }
}
