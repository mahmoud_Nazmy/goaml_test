/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

import com.goaml.dao.FilesDao;
import com.goaml.dao.GeneralData;
import com.goaml.dao.UploadFilesDao;
import com.goaml.logger.LogControl;
import com.goaml.model.Accounts;
import com.goaml.model.CustomersHdb;
import com.goaml.model.FtTrans;
import com.goaml.model.TtTrans;
import com.goaml.model.UploadedFiles;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 *
 * @author Mahmoud
 */
public class ReadFilesUtil {

    FilesDao dao;
    GeneralData generalData;

    public ReadFilesUtil() {
        dao = new FilesDao();
        generalData = new GeneralData();
    }

    public String readCustomerFile(String cusfile) {
        String fileName = null;
        String status = "";
        int rowCount = 0;
        BufferedReader br;
        try {
            File file = new File(cusfile);
            fileName = file.getName();
            String line;
            String cvsSplitBy = generalData.getSysParam("FilesDelimeter");
//            BufferedReader br = null;

//            br = new BufferedReader(new FileReader(file));
//             BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"));
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file), "Cp1252"));
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                rowCount++;
                try {

                    String[] recid = line.split(cvsSplitBy);
                    String cusrecid = recid[0];
                    String gender = recid[1];
                    String title = recid[2];
                    String name_Ar = recid[3];
                    String name_En = recid[4];
                    String familyName = recid[5];
                    String fatherName = recid[6];
                    String legalId = recid[7];
                    String LEGAL_ISS_DATE = recid[8];
                    String LEGAL_EXP_DATE = recid[9];
                    String LEGAL_DOC_NAME = recid[10];
                    String NATIONALITY = recid[11];
                    String DATE_OF_BIRTH = recid[12];
                    String ADDRESS = recid[13];
                    String ADDR_LOCATION = recid[14];
                    String COUNTRY = recid[15];
                    String STREET = recid[16];
                    String JOB_TITLE = recid[17];
                    String EMPLOYMENT_STATUS = recid[18];
                    String EDUCATION_LEVEL = recid[19];
                    String sector = recid[20];
                    String SHORT_NAME = recid[21];
                    String SMS = recid[22];
                    CustomersHdb customers = new CustomersHdb();
                    customers.setRecid(cusrecid);
                    customers.setGender(gender);
                    customers.setTitle(title);
                    customers.setNameAr(name_Ar);
                    customers.setNameEn(name_En);
                    customers.setFamilyName(familyName);
                    customers.setFatherName(fatherName);
                    customers.setLegalId(legalId);
                    customers.setLegalIssDate(LEGAL_ISS_DATE);
                    customers.setLegalExpDate(LEGAL_EXP_DATE);
                    customers.setLegalIdDocName(LEGAL_DOC_NAME);
                    customers.setNationality(NATIONALITY);
                    customers.setDateOfBirth(DATE_OF_BIRTH);
                    customers.setAddress(ADDRESS);
                    customers.setAddrLocation(ADDR_LOCATION);
                    customers.setCountry(COUNTRY);
                    customers.setStreet(STREET);
                    customers.setJobTitle(JOB_TITLE);
                    customers.setEmploymentStatus71(EMPLOYMENT_STATUS);
                    customers.setEducationLevel(EDUCATION_LEVEL);
                    customers.setSector(sector);
                    customers.setShortName(SHORT_NAME);
                    customers.setSms(SMS);
                    dao.saveFile(customers);
                    status = "uploaded";
                    UploadFilesDao dao = new UploadFilesDao();
                    UploadedFiles files = new UploadedFiles();
                    files.setFileName(fileName);
                    files.setTotalRows(new BigDecimal(rowCount));
                    files.setStatus(status);
                    dao.saveFile(files);
                    System.out.println(cusrecid);
                } catch (Exception ex) {

                    ex.printStackTrace();
                    LogControl.error(ex.getMessage());
                    continue;

                }
            }
            status = "uploaded";
        } catch (Exception ex) {
            status = "error";
            UploadFilesDao dao = new UploadFilesDao();
            UploadedFiles files = new UploadedFiles();
            files.setFileName(fileName);
            files.setTotalRows(new BigDecimal(rowCount));
            files.setStatus(status);
            dao.saveFile(files);
        }

        return status;
    }

    public String uploadAccountsFile(String accFile) {
        String status = "";
        int rowCount = 0;
        BufferedReader br;
        try {
            File file = new File(accFile);
            String line;
            String cvsSplitBy = generalData.getSysParam("FilesDelimeter");
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file), "UTF-8"));
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                rowCount++;
                try {

                    String[] record = line.split(cvsSplitBy);
                    String RECID = record[0];
                    String customerNo = record[1];
                    String SHORT_TITLE = record[2];
                    String CURRENCY = record[3];
                    String balance = record[4];
                    String TRAN_LAST_DR_CUST = record[5];
                    String TRAN_LAST_CR_CUST = record[6];
                    String dateLastDrCust = record[7];
                    String dateLastCrCust = record[8];
                    String CATEGORY = record[9];
                    String accStatus = record[10];
                    String signatory = record[11];
                    String opening_date = record[12];
                    String coCode = record[13];
                    Accounts accounts = new Accounts();
                    accounts.setRecid(RECID);
                    accounts.setCustomerNo(customerNo);
                    accounts.setShortTitle(SHORT_TITLE);
                    accounts.setCurrency(CURRENCY);
                    if (balance.equalsIgnoreCase("")) {
                        balance = "0";
                    }
                    accounts.setBalance(new BigDecimal(balance));
                    accounts.setTranLastDrCust(new BigDecimal(TRAN_LAST_DR_CUST));
                    accounts.setTranLastCrCust(new BigDecimal(TRAN_LAST_CR_CUST));
                    accounts.setDateLastCrCust(dateLastCrCust);
                    accounts.setDateLastDrCust(dateLastDrCust);
                    accounts.setCategory(CATEGORY);
                    accounts.setStatus(accStatus);
                    accounts.setSignatory(signatory);
                    accounts.setOpeningDate(opening_date);
                    accounts.setCoCode(coCode);
                    dao.saveAccountsFile(accounts);
                    status = "uploaded";
                    UploadFilesDao udao = new UploadFilesDao();

                    UploadedFiles files = new UploadedFiles();
                    files.setFileName(file.getName());
                    files.setTotalRows(new BigDecimal(rowCount));
                    files.setStatus(status);
                    udao.saveFile(files);
                    System.out.println(RECID);
                } catch (Exception ex) {

                    ex.printStackTrace();
                    LogControl.error(ex.getMessage());
                    continue;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            status = "Error";
        }
        return status;
    }

    public String uploadFTransactions(String FTFile) {
        String status = "";
        int rowCount = 0;
        BufferedReader br;
        try {
            File file = new File(FTFile);
            String line;
            String cvsSplitBy = generalData.getSysParam("FilesDelimeter");
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file), "Cp1252"));
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                rowCount++;
                try {

                    String[] record = line.split(cvsSplitBy);
                    String RECID = record[0];
                    String TRANSACTION_TYPE = record[1];
                    String DEBIT_ACCT_NO = record[2];
                    String DEBIT_CURRENCY = record[3];
                    String DEBIT_AMOUNT = record[4];
                    String DEBIT_VALUE_DATE = record[5];
                    String DEBIT_THEIR_REF = record[6];
                    String CREDIT_ACCT_NO = record[7];
                    String CREDIT_CURRENCY = record[8];
                    String CREDIT_AMOUNT = record[9];
                    String CREDIT_VALUE_DATE = record[10];
                    String PROCESSING_DATE = record[11];
                    String DESCRRIPTION = record[12];
                    String INPUTTER = record[13];
                    String AUTHORISER = record[14];
                    String CO_CODE = record[15];
                    FtTrans ftTrans = new FtTrans();
                    ftTrans.setRecid(RECID);
                    ftTrans.setTransactionType(TRANSACTION_TYPE);
                    ftTrans.setDebitAcctNo(DEBIT_ACCT_NO);
                    ftTrans.setDebitCurrency(DEBIT_CURRENCY);
                    if (DEBIT_AMOUNT.equalsIgnoreCase("")) {
                        DEBIT_AMOUNT = "0";
                    }
                    ftTrans.setDebitAmount(new BigDecimal(DEBIT_AMOUNT));
                    ftTrans.setDebitValueDate(DEBIT_VALUE_DATE);
                    ftTrans.setDebitTheirRef(DEBIT_THEIR_REF);
                    ftTrans.setCreditAcctNo(CREDIT_ACCT_NO);
                    ftTrans.setCreditCurrency(CREDIT_CURRENCY);
                    ftTrans.setCreditAmount(new BigDecimal(CREDIT_AMOUNT));
                    ftTrans.setCreditValueDate(CREDIT_VALUE_DATE);
                    ftTrans.setProcessingDate(PROCESSING_DATE);
                    ftTrans.setDescrription(DESCRRIPTION);
                    ftTrans.setInputter(INPUTTER);
                    ftTrans.setAuthoriser(AUTHORISER);
                    ftTrans.setCoCode(CO_CODE);
                    dao.saveFTFiles(ftTrans);
                    status = "uploaded";
                    UploadFilesDao udaodao = new UploadFilesDao();
                    UploadedFiles files = new UploadedFiles();
                    files.setFileName(file.getName());
                    files.setTotalRows(new BigDecimal(rowCount));
                    files.setStatus(status);
                    udaodao.saveFile(files);
//                    System.out.println(RECID);
                } catch (Exception ex) {

                    ex.printStackTrace();
                    LogControl.error(ex.getMessage());
                    continue;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            UploadFilesDao udaodao = new UploadFilesDao();
            UploadedFiles files = new UploadedFiles();
            files.setFileName(FTFile);
            files.setStatus(status);
            udaodao.saveFile(files);
            status = "Error";
        }
        return status;
    }

    public String uploadTTransactions(String TTFile) {
        String status = "";
        int rowCount = 0;
        BufferedReader br;
        try {
            File file = new File(TTFile);
            String line;
            String cvsSplitBy = generalData.getSysParam("FilesDelimeter");
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file), "UTF-8"));
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                rowCount++;
                try {

                    String[] record = line.split(cvsSplitBy);

                    String RECID = record[0];
                    String ACCOUNT_1 = record[1];
                    String ACCOUNT_2 = record[2];
                    String DR_CR_MARKER = record[3];
                    String AMOUNT_LOCAL_1 = record[4];
                    String AMOUNT_LOCAL_2 = record[5];
                    String AMOUNT_FCY_1 = record[6];
                    String CURRENCY_1 = record[7];
                    String CURRENCY_2 = record[8];
                    String DEPOSITER_ID = record[9];
                    String DEPOSITER_NAME = record[10];
                    String TELLER_ID_1 = record[11];
                    String INPUTTER = record[12];
                    String AUTHORISER = record[13];
                    String CO_CODE = record[14];
                    String TRANSACTION_TYPE = record[15];
                    TtTrans tt = new TtTrans();
                    tt.setRecid(RECID);
                    tt.setAccount1(ACCOUNT_1);
                    tt.setAccount2(ACCOUNT_2);
                    tt.setDrCrMarker(DR_CR_MARKER);
                    if (AMOUNT_LOCAL_1.equalsIgnoreCase("")) {
                        AMOUNT_LOCAL_1 = "0";
                    }
                    tt.setAmountLocal1(new BigDecimal(AMOUNT_LOCAL_1));
                    if (AMOUNT_LOCAL_2.equalsIgnoreCase("")) {
                        AMOUNT_LOCAL_2 = "0";
                    }
                    tt.setAmountLocal2(new BigDecimal(AMOUNT_LOCAL_2));
                    if (AMOUNT_FCY_1.equalsIgnoreCase("")) {
                        AMOUNT_FCY_1 = "0";
                    }
                    tt.setAmountFcy1(new BigDecimal(AMOUNT_FCY_1));
                    tt.setCurrency1(CURRENCY_1);
                    tt.setCurrency2(CURRENCY_2);
                    tt.setDepositerId(DEPOSITER_ID);
                    tt.setDepositerName(DEPOSITER_NAME);
                    tt.setTellerId1(TELLER_ID_1);
                    tt.setInputter(INPUTTER);
                    tt.setAuthoriser(AUTHORISER);
                    tt.setCoCode(CO_CODE);
                    tt.setTransactionType(TRANSACTION_TYPE);

                    dao.saveTTfile(tt);
                    status = "uploaded";
                    UploadFilesDao udaodao = new UploadFilesDao();
                    UploadedFiles files = new UploadedFiles();
                    files.setFileName(file.getName());
                    files.setTotalRows(new BigDecimal(rowCount));
                    files.setStatus(status);
                    udaodao.saveFile(files);
//                    System.out.println(RECID);
                } catch (Exception ex) {
                    ex.getMessage();
                    ex.printStackTrace();
                    LogControl.error(ex.getMessage());
                    continue;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            ex.printStackTrace();
//            UploadFilesDao udaodao = new UploadFilesDao();
//            UploadedFiles files = new UploadedFiles();
//            files.setFileName(file.getName());
//            files.setStatus(status);
//            udaodao.saveFile(files);
            status = "Error";
        }
        return status;
    }
}
