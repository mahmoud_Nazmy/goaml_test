/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;


//import com.smart.model.Usersecurity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author M.Nazmy
 */
public class HandelSession {

    FacesContext context ;
    HttpServletRequest request ;
    HttpSession httpSession ;

    public HandelSession() {
    }

    public  void setAttribute(String name, Object o) {
        context = FacesContext.getCurrentInstance();
        request = (HttpServletRequest) context.getExternalContext().getRequest();
        httpSession = request.getSession(true);
        httpSession.setAttribute(name, o);
    }

    public  Object getAttribute(String name) {
        Object obj;
        try{
            obj  = (Object) FacesContext.getCurrentInstance().
		getExternalContext().getSessionMap().get(name);
        } catch (IllegalStateException illegalStateException){
         obj=null;  
        }
        return obj;
    }

    public  String getOPECode() {
//      Usersecurity user = (Usersecurity) HandelSession.getAttribute("user");
//       if (user == null) {
//            return "Error";
//        } else {
//            String opeCode = user.getUsername();
//            return opeCode;
//        }
        return "";
    }
}
