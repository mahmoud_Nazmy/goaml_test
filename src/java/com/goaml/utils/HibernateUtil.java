/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

import java.sql.Connection;
import java.sql.SQLException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.SessionImpl;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author M.Nazmy
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        try {

            if (sessionFactory == null) {
                // loads configuration and mappings
                Configuration configuration = new Configuration();
                configuration.configure();
                sessionFactory = new Configuration().configure().buildSessionFactory();
                System.out.println("sessionFactory" + sessionFactory);
            }

            return sessionFactory;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Initial SessionFactory creation failed." + ex);

            throw new ExceptionInInitializerError(ex.getMessage());
        }
    }

    public static Connection getConnection() throws SQLException {
        try {
            SessionImpl si = (SessionImpl) HibernateUtil.getSessionFactory().openSession();
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) HibernateUtil.getSessionFactory(); //session.getSessionFactory();
            // ConnectionProvider cp = sfi.getConnectionProvider();
            Connection connection = si.connection();
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("******Error Connecting*****" + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) throws SQLException {
        getSessionFactory();
        Connection c = getConnection();
        System.out.println("::" + c.getSchema());

    }
}
