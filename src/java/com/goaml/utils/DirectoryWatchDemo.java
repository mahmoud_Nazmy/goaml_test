/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

/**
 *
 * @author Mahmoud
 */
import com.goaml.dao.UploadFilesDao;
import com.goaml.logger.Log;
import com.goaml.logger.LogControl;
import com.goaml.model.UploadedFiles;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.sql.SQLException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * This program demonstrates how to use the Watch Service API to monitor change
 * events for a specific directory.
 *
 * @author www.codejava.net
 *
 */
public class DirectoryWatchDemo {

    final static Logger logger = Logger.getLogger(DirectoryWatchDemo.class.getName());

//    public static void main(String[] args) throws SQLException, InterruptedException {
//
//        Path dir = null;
//        String listnerPath = "E://ListnerTest//";
//        ReadFilesUtil filesUtil = new ReadFilesUtil();
//        try {
//            WatchService watcher = FileSystems.getDefault().newWatchService();
//            dir = Paths.get(listnerPath);
//            dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
////            LogControl.error("Infoooooooo:::::  ");
//            System.out.println("Watch Service registered for dir: " + dir.getFileName());
//
//            while (true) {
//                WatchKey key;
//                try {
//                    key = watcher.take();
//                } catch (InterruptedException ex) {
//                    return;
//                }
//
//                for (WatchEvent<?> event : key.pollEvents()) {
//                    WatchEvent.Kind<?> kind = event.kind();
//
//                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
//                    Path fileName = ev.context();
//
//                    System.out.println(kind.name() + ": " + fileName);
//
//                    if (kind == ENTRY_CREATE) {
//                        logger.debug("Hello this is a debug message");
//                        logger.info("Hello this is an info message");
//
//                        Thread.sleep(1000);
//                        String filePath = listnerPath + "//" + fileName;
//
//                        if (fileName.toString().startsWith("cus")) {
////                            System.out.println("Customer file identified");
//                            logger.info("Customer file identified");
//                            try {
//                                String uploadingStatus = filesUtil.readCustomerFile(filePath);
////                                System.out.println(uploadingStatus);
////                                System.out.println(" file has been Uploaded!!!");
//
//                            } catch (Exception ex) {
//                                ex.printStackTrace();
//                                logger.error(ex.getMessage());
//                            }
//                        } else if (fileName.toString().startsWith("acc")) {
////                            System.out.println("");
////                            System.out.println("accounts file identified");
//                            try {
//                                String uploadingStatus = filesUtil.uploadFTransactions(filePath);
//                                System.out.println(uploadingStatus);
//                                System.out.println(" file has been Uploaded!!!");
//
//                            } catch (Exception ex) {
////                                Log.error(ex.getMessage());
//                                ex.printStackTrace();
//                            }
//                        }
//
//                    }
//
////                    if (kind == ENTRY_MODIFY
////                            && fileName.toString().equals("DirectoryWatchDemo.java")) {
////                        System.out.println("My source file has changed!!!");
////                        UploadFilesDao dao = new UploadFilesDao();
////                        UploadedFiles files = new UploadedFiles();
////                        files.setFileName(fileName.toString());
////                        files.setTotalRows(BigDecimal.ZERO);
////                    }
//                }
//
//                boolean valid = key.reset();
//                if (!valid) {
//                    break;
//                }
//            }
//            logger.error("Infoooooooo:::::  ");
//        } catch (IOException ex) {
//            System.err.println(ex);
//        }
//
//    }
public static void main(String[] args) throws InterruptedException {
        watchService();
    }
    public static void watchService() throws InterruptedException {
        Path dir = null;
        String listnerPath = "E://ListnerTest//";
        ReadFilesUtil filesUtil = new ReadFilesUtil();
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            dir = Paths.get(listnerPath);
            dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
//            LogControl.error("Infoooooooo:::::  ");
            System.out.println("Watch Service registered for dir: " + dir.getFileName());

            while (true) {
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException ex) {
                    return;
                }

                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();

                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    Path fileName = ev.context();

                    System.out.println(kind.name() + ": " + fileName);

                    if (kind == ENTRY_CREATE) {
                        logger.debug("Hello this is a debug message");
                        logger.info("Hello this is an info message");

                        Thread.sleep(1000);
                        String filePath = listnerPath + "//" + fileName;

                        if (fileName.toString().startsWith("cus")) {
//                            System.out.println("Customer file identified");
                            logger.info("Customer file identified");
                            try {
                                String uploadingStatus = filesUtil.readCustomerFile(filePath);
//                                System.out.println(uploadingStatus);
//                                System.out.println(" file has been Uploaded!!!");

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                logger.error(ex.getMessage());
                            }
                        } else if (fileName.toString().startsWith("acc")) {
//                            System.out.println("");
//                            System.out.println("accounts file identified");
                            try {
                                String uploadingStatus = filesUtil.uploadAccountsFile(filePath);
                                System.out.println(uploadingStatus);
                                System.out.println(" file has been Uploaded!!!");

                            } catch (Exception ex) {
//                                Log.error(ex.getMessage());
                                ex.printStackTrace();
                            }
                        }else if (fileName.toString().startsWith("tt")) {
//                            System.out.println("");
//                            System.out.println("accounts file identified");
                            try {
                                String uploadingStatus = filesUtil.uploadTTransactions(filePath);
                                System.out.println(uploadingStatus);
                                System.out.println(" file has been Uploaded!!!");

                            } catch (Exception ex) {
//                                Log.error(ex.getMessage());
                                ex.printStackTrace();
                            }
                        }

                    }

//                    if (kind == ENTRY_MODIFY
//                            && fileName.toString().equals("DirectoryWatchDemo.java")) {
//                        System.out.println("My source file has changed!!!");
//                        UploadFilesDao dao = new UploadFilesDao();
//                        UploadedFiles files = new UploadedFiles();
//                        files.setFileName(fileName.toString());
//                        files.setTotalRows(BigDecimal.ZERO);
//                    }
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }
            logger.error("Infoooooooo:::::  ");
        } catch (IOException ex) {
            System.err.println(ex);
        }

    }
}
