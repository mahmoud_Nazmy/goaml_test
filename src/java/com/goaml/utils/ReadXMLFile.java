/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

/**
 *
 * @author Mahmoud
 */
import com.goaml.model.FtFirst;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Text;

public class ReadXMLFile {

    public static void main(String argv[]) {

        try {

            File fXmlFile = new File("E:\\xml\\1.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
//            Node company = doc.getFirstChild();
            Node staff = doc.getElementsByTagName("*").item(0);
            NodeList list = staff.getChildNodes();

            for (int i = 0; i < list.getLength(); i++) {

                Node node = list.item(i);

                // get the salary element, and update the value
                if ("report_code".equals(node.getNodeName())) {
                    node.setTextContent("SAR");
                }
                if ("entity_reference".equals(node.getNodeName())) {
                    node.setTextContent("16001");
                }

                if ("submission_date".equals(node.getNodeName())) {
                    node.setTextContent("today");
                }
                if ("currency_code_local".equals(node.getNodeName())) {
                    node.setTextContent("USD");
                }

            }
            NodeList nList3 = doc.getElementsByTagName("*");
            for (int temp = 0; temp < nList3.getLength(); temp++) {

                Node nNode = nList3.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    System.out.println("aaaaaaaa" + nNode.getNodeName());
                    if ("gender".equals(nNode.getNodeName())) {

                        nNode.setTextContent("100220000");
                    }
                    if ("title".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("first_name".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("last_name".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("birthdate".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("ssn".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("nationality1".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }

                }
            }
            Node tph_contact_type = doc.getElementsByTagName("tph_contact_type").item(0);
            tph_contact_type.setTextContent("qqq");

            Node tph_communication_type = doc.getElementsByTagName("tph_communication_type").item(0);
            tph_communication_type.setTextContent("qqq");

            Node tph_number = doc.getElementsByTagName("tph_number").item(0);
            tph_number.setTextContent("qqq");

            Node address_type = doc.getElementsByTagName("address_type").item(0);
            address_type.setTextContent("qqq");

            Node city = doc.getElementsByTagName("city").item(0);
            city.setTextContent("qqq");

            Node country_code = doc.getElementsByTagName("country_code").item(0);
            country_code.setTextContent("qqq");

            Node state = doc.getElementsByTagName("state").item(0);
            state.setTextContent("qqq");

            Node occupation = doc.getElementsByTagName("occupation").item(0);
            occupation.setTextContent("occupation");

            Node address_type2 = doc.getElementsByTagName("address_type").item(1);
            address_type2.setTextContent("address_type22");

            Node address = doc.getElementsByTagName("address").item(1);
            address.setTextContent("addresssss");

            NodeList nodes = doc.getElementsByTagName("report_indicators");
///////////////////////////////////////////////////////////////////////////////Add new transaction block/////////////////////
            Element node = doc.createElement("transaction");
            ///////////////////////transactionnumber///////////////
            Element tran = doc.createElement("transactionnumber");
            Text a = doc.createTextNode("111");
            tran.appendChild(a);
            node.appendChild(tran);
            ///////////////////////transaction_location///////////////
            Element tranLocation = doc.createElement("transaction_location");
            Text location = doc.createTextNode("ssss");
            tranLocation.appendChild(location);
            node.appendChild(tranLocation);
            ///////////////////////transaction_description///////////////
            Element transDesc = doc.createElement("transaction_description");
            Text desc = doc.createTextNode("deposit");
            transDesc.appendChild(desc);
            node.appendChild(transDesc);
            /////////////////////////////////

            ///////////////////////date_transaction///////////////
            Element dateTran = doc.createElement("date_transaction");
            Text date = doc.createTextNode("deposit");
            dateTran.appendChild(date);
            node.appendChild(dateTran);
            ///////////////////////transmode_code///////////////
            Element transmodeCode = doc.createElement("transmode_code");
            Text code = doc.createTextNode("D");
            transmodeCode.appendChild(code);
            node.appendChild(transmodeCode);
            ///////////////////////amount_local///////////////
            Element amount_local2 = doc.createElement("amount_local");
            Text amount = doc.createTextNode("1000");
            amount_local2.appendChild(amount);
            node.appendChild(amount_local2);
            /////////////////////////////////
            Element tfrom = doc.createElement("t_from_my_client");
            node.appendChild(tfrom);
            ///////////////////////from_funds_code/////////////
            Element from_funds_code = doc.createElement("from_funds_code");
            Text from_funds = doc.createTextNode("C");
            from_funds_code.appendChild(from_funds);
            tfrom.appendChild(from_funds_code);
            ////////////////////////////////from_person///////////////
            Element fromP = doc.createElement("from_person");
            tfrom.appendChild(fromP);
            ////////////////////////////////gender///////////////fromPerson
            Element gender = doc.createElement("gender");
            Text gen = doc.createTextNode("gender");
            gender.appendChild(gen);
            fromP.appendChild(gender);
            //////////////////////////////Title//////////////////
            Element Title = doc.createElement("title");
            Text title = doc.createTextNode("MR/");
            Title.appendChild(title);
            fromP.appendChild(Title);
            //////////////////////////////firstName/////////////
            Element firstName = doc.createElement("first_name");
            Text firstname = doc.createTextNode("test");
            firstName.appendChild(firstname);
            fromP.appendChild(firstName);
            //////////////////////////////middle_name//////////////
            Element middle_name = doc.createElement("middle_name");
            Text middlename = doc.createTextNode("test");
            middle_name.appendChild(middlename);
            fromP.appendChild(middle_name);
            //////////////////////////////prefix//////////////
            Element prefix = doc.createElement("prefix");
            Text prefixx = doc.createTextNode("test Bardo");
            prefix.appendChild(prefixx);
            fromP.appendChild(prefix);
            //////////////////////////////last_name//////////////
            Element last_name = doc.createElement("last_name");
            Text lastName = doc.createTextNode("test Bardo");
            last_name.appendChild(lastName);
            fromP.appendChild(last_name);
            //////////////////////////////birthdate//////////////
            Element birthdate = doc.createElement("birthdate");
            Text birthDate = doc.createTextNode("1984-11-16T00:00:00");
            birthdate.appendChild(birthDate);
            fromP.appendChild(birthdate);
            //////////////////////////////ssn//////////////
            Element ssn = doc.createElement("ssn");
            Text nid = doc.createTextNode("28705");
            ssn.appendChild(nid);
            fromP.appendChild(ssn);
            //////////////////////////////nationality1//////////////
            Element nationality1 = doc.createElement("nationality1");
            Text nationality = doc.createTextNode("28705");
            nationality1.appendChild(nationality);
            fromP.appendChild(nationality1);
            //////////////////////////////residence//////////////
            Element residence = doc.createElement("residence");
            Text residen = doc.createTextNode("EG");
            residence.appendChild(residen);
            fromP.appendChild(residence);
            ///////////////////////////////Phones
            Element phones = doc.createElement("phones");
            tfrom.appendChild(phones);
            /////////////////////////////phone//////////////
            Element phone = doc.createElement("phone");
            phones.appendChild(phone);
            /////////////////////////////tph_contact_type>

            Element tph_contact = doc.createElement("tph_contact_type");
            Text tph_contactt = doc.createTextNode("P");
            tph_contact.appendChild(tph_contactt);
            phone.appendChild(tph_contact);
            ///////////////////////////tph_communication_type///////////
            Element tph_communication_typ = doc.createElement("tph_communication_type");
            Text tph_communicationtyp = doc.createTextNode("M");
            tph_communication_typ.appendChild(tph_communicationtyp);
            phone.appendChild(tph_communication_typ);
            ///////////////////////////tph_number/////////////////////
            Element tph_Number = doc.createElement("tph_number");
            Text tphNumber = doc.createTextNode("010000000000000");
            tph_Number.appendChild(tphNumber);
            phone.appendChild(tph_Number);
            //////////////////////////////-<addresses
            Element addresses = doc.createElement("addresses");
            tfrom.appendChild(addresses);
            ///////////////////////////////////-<address
            Element address2 = doc.createElement("address");
            addresses.appendChild(address2);
            ///////////////////////////////address_type
            Element addressType = doc.createElement("address_type");
            Text addressTypee = doc.createTextNode("AID");
            addressType.appendChild(addressTypee);
            address2.appendChild(addressType);
            /////////////////////////////////////address
            Element addres = doc.createElement("address");
            Text addresT = doc.createTextNode("Tawabe2");
            addres.appendChild(addresT);
            address2.appendChild(addres);
            /////////////////////////////////////town///////////
            Element town = doc.createElement("town");
            Text townT = doc.createTextNode("faysl");
            town.appendChild(townT);
            address2.appendChild(town);
            /////////////////////////////////////city
            Element city2 = doc.createElement("city");
            Text city2T = doc.createTextNode("haram");
            city2.appendChild(city2T);
            address2.appendChild(city2);
            ////////////////////////////////////country_code
            Element countryCode = doc.createElement("country_code");
            Text countryCodeT = doc.createTextNode("EG");
            countryCode.appendChild(countryCodeT);
            address2.appendChild(countryCode);
            ////////////////////////////////////state
            Element state2 = doc.createElement("state");
            Text state2T = doc.createTextNode("Jiza");
            state2.appendChild(state2T);
            address2.appendChild(state2);
            //////////////////////////////////occupation
            Element occupation2 = doc.createElement("occupation");
            Text occupation2T = doc.createTextNode("Commerce");
            occupation2.appendChild(occupation2T);
            tfrom.appendChild(occupation2);
            /////////////////////////////////-<identification
            Element identification = doc.createElement("identification");
            tfrom.appendChild(identification);
            /////////////////////////////////-<identification
            Element type = doc.createElement("type");
            Text typeT = doc.createTextNode("NID");
            type.appendChild(typeT);
            identification.appendChild(type);
            /////////////////////////////////number
            Element number = doc.createElement("number");
            Text numberT = doc.createTextNode("287**********");
            number.appendChild(numberT);
            identification.appendChild(number);
            ////////////////////////////////////expiry_date
            Element expiry_date = doc.createElement("expiry_date");
            Text expiry_dateT = doc.createTextNode("2017-10-25T00:00:00");
            expiry_date.appendChild(expiry_dateT);
            identification.appendChild(expiry_date);
            /////////////////////////////////////issue_country
            Element issue_country = doc.createElement("issue_country");
            Text issue_countryT = doc.createTextNode("EG");
            issue_country.appendChild(issue_countryT);
            identification.appendChild(issue_country);

            /////////////////////////////////////////////////To_My_Client//////////////////////
            Element t_to_my_client = doc.createElement("t_to_my_client");
            node.appendChild(t_to_my_client);
            ///////////////////////from_funds_code/////////////
            Element my_from_funds_code = doc.createElement("to_from_funds_code");
            Text my_from_funds = doc.createTextNode("C");
            my_from_funds_code.appendChild(my_from_funds);
            t_to_my_client.appendChild(my_from_funds_code);
            ////////////////////////////////from_person///////////////
            Element my_fromP = doc.createElement("t_person");
            t_to_my_client.appendChild(my_fromP);
            ////////////////////////////////gender///////////////fromPerson
            Element my_gender = doc.createElement("gender");
            Text my_gen = doc.createTextNode("gender");
            my_gender.appendChild(my_gen);
            my_fromP.appendChild(my_gender);
            //////////////////////////////Title//////////////////
            Element my_Title = doc.createElement("title");
            Text my_title = doc.createTextNode("MR/");
            my_Title.appendChild(my_title);
            my_fromP.appendChild(my_Title);
            //////////////////////////////firstName/////////////
            Element my_firstName = doc.createElement("first_name");
            Text my_firstname = doc.createTextNode("test");
            my_firstName.appendChild(my_firstname);
            my_fromP.appendChild(my_firstName);
            //////////////////////////////middle_name//////////////
            Element my_middle_name = doc.createElement("middle_name");
            Text my_middlename = doc.createTextNode("test");
            my_middle_name.appendChild(my_middlename);
            my_fromP.appendChild(my_middle_name);
            //////////////////////////////prefix//////////////
            Element my_prefix = doc.createElement("prefix");
            Text my_prefixx = doc.createTextNode("test Bardo");
            my_prefix.appendChild(my_prefixx);
            my_fromP.appendChild(my_prefix);
            //////////////////////////////last_name//////////////
            Element my_last_name = doc.createElement("last_name");
            Text my_lastName = doc.createTextNode("test Bardo");
            my_last_name.appendChild(my_lastName);
            my_fromP.appendChild(my_last_name);
            //////////////////////////////birthdate//////////////
            Element my_birthdate = doc.createElement("birthdate");
            Text my_birthDate = doc.createTextNode("1984-11-16T00:00:00");
            my_birthdate.appendChild(my_birthDate);
            my_fromP.appendChild(my_birthdate);
            //////////////////////////////ssn//////////////
            Element my_ssn = doc.createElement("ssn");
            Text my_nid = doc.createTextNode("28705");
            my_ssn.appendChild(my_nid);
            my_fromP.appendChild(my_ssn);
            //////////////////////////////nationality1//////////////
            Element my_nationality1 = doc.createElement("nationality1");
            Text my_nationality = doc.createTextNode("28705");
            my_nationality1.appendChild(my_nationality);
            my_fromP.appendChild(my_nationality1);
            //////////////////////////////residence//////////////
            Element my_residence = doc.createElement("residence");
            Text my_residen = doc.createTextNode("EG");
            my_residence.appendChild(my_residen);
            my_fromP.appendChild(my_residence);
            ///////////////////////////////Phones
            Element my_phones = doc.createElement("phones");
            t_to_my_client.appendChild(my_phones);
            /////////////////////////////phone//////////////
            Element my_phone = doc.createElement("phone");
            my_phones.appendChild(my_phone);
            /////////////////////////////tph_contact_type>

            Element my_tph_contact = doc.createElement("tph_contact_type");
            Text my_tph_contactt = doc.createTextNode("P");
            my_tph_contact.appendChild(my_tph_contactt);
            my_phone.appendChild(my_tph_contact);
            ///////////////////////////tph_communication_type///////////
            Element my_tph_communication_typ = doc.createElement("tph_communication_type");
            Text tph_my_communicationtyp = doc.createTextNode("M");
            my_tph_communication_typ.appendChild(tph_my_communicationtyp);
            my_phone.appendChild(my_tph_communication_typ);
            ///////////////////////////tph_number/////////////////////
            Element my_tph_Number = doc.createElement("tph_number");
            Text my_tphNumber = doc.createTextNode("010000000000000");
            my_tph_Number.appendChild(my_tphNumber);
            my_phone.appendChild(my_tph_Number);
            //////////////////////////////-<addresses
            Element my_addresses = doc.createElement("addresses");
            t_to_my_client.appendChild(my_addresses);
            ///////////////////////////////////-<address
            Element my_address2 = doc.createElement("address");
            my_addresses.appendChild(my_address2);
            ///////////////////////////////address_type
            Element my_addressType = doc.createElement("address_type");
            Text my_addressTypee = doc.createTextNode("AID");
            my_addressType.appendChild(my_addressTypee);
            my_addresses.appendChild(my_addressType);
            /////////////////////////////////////address
            Element my_addres = doc.createElement("address");
            Text my_addresT = doc.createTextNode("Tawabe2");
            my_addres.appendChild(my_addresT);
            my_address2.appendChild(my_addres);
            /////////////////////////////////////town///////////
            Element my_town = doc.createElement("town");
            Text my_townT = doc.createTextNode("faysl");
            my_town.appendChild(my_townT);
            my_address2.appendChild(my_town);
            /////////////////////////////////////city
            Element my_city2 = doc.createElement("city");
            Text my_city2T = doc.createTextNode("haram");
            my_city2.appendChild(my_city2T);
            my_address2.appendChild(my_city2);
            ////////////////////////////////////country_code
            Element my_countryCode = doc.createElement("country_code");
            Text my_countryCodeT = doc.createTextNode("EG");
            my_countryCode.appendChild(my_countryCodeT);
            my_address2.appendChild(my_countryCode);
            ////////////////////////////////////state
            Element my_state2 = doc.createElement("state");
            Text my_state2T = doc.createTextNode("Jiza");
            my_state2.appendChild(my_state2T);
            my_address2.appendChild(my_state2);
            //////////////////////////////////occupation
            Element my_occupation2 = doc.createElement("occupation");
            Text my_occupation2T = doc.createTextNode("Commerce");
            my_occupation2.appendChild(my_occupation2T);
            tfrom.appendChild(my_occupation2);
            /////////////////////////////////-<identification
            Element my_identification = doc.createElement("identification");
            t_to_my_client.appendChild(my_identification);
            /////////////////////////////////-<identification
            Element my_type = doc.createElement("type");
            Text my_typeT = doc.createTextNode("NID");
            my_type.appendChild(my_typeT);
            my_identification.appendChild(my_type);
            /////////////////////////////////number
            Element my_number = doc.createElement("number");
            Text my_numberT = doc.createTextNode("287**********");
            my_number.appendChild(my_numberT);
            my_identification.appendChild(my_number);
            ////////////////////////////////////expiry_date
            Element my_expiry_date = doc.createElement("expiry_date");
            Text my_expiry_dateT = doc.createTextNode("2017-10-25T00:00:00");
            my_expiry_date.appendChild(my_expiry_dateT);
            my_identification.appendChild(my_expiry_date);
            /////////////////////////////////////issue_country
            Element my_issue_country = doc.createElement("issue_country");
            Text my_issue_countryT = doc.createTextNode("EG");
            my_issue_country.appendChild(my_issue_countryT);
            my_identification.appendChild(my_issue_country);

            nodes.item(0).getParentNode().insertBefore(node, nodes.item(0));

            ////////////////////////////////////////////// End add Transaction---------------------------------
            System.out.println("----------------------------");

            Node transactionnumber = doc.getElementsByTagName("transactionnumber").item(0);
            transactionnumber.setTextContent("qqq");
            Node transaction_location = doc.getElementsByTagName("transaction_location").item(0);
            transaction_location.setTextContent("qqq");
            Node transaction_description = doc.getElementsByTagName("transaction_description").item(0);
            transaction_description.setTextContent("transaction_description");
            Node date_transaction = doc.getElementsByTagName("date_transaction").item(0);
            date_transaction.setTextContent("date_transaction");
            Node teller = doc.getElementsByTagName("teller").item(0);
            teller.setTextContent("teller");
            Node authorized = doc.getElementsByTagName("authorized").item(0);
            authorized.setTextContent("authorized");
            Node transmode_code = doc.getElementsByTagName("transmode_code").item(0);
            transmode_code.setTextContent("transmode_code");
            Node amount_local = doc.getElementsByTagName("amount_local").item(0);
            amount_local.setTextContent("amount_local");
            NodeList nList2 = doc.getElementsByTagName("amount_local");
            for (int temp = 0; temp < nList2.getLength(); temp++) {

                Node nNode = nList2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    System.out.println("aaaaaaaa" + nNode.getNodeName());
                    if ("amount_local".equals(nNode.getNodeName())) {

                        nNode.setTextContent("00000");
                    }
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("E:\\xml\\1.xml"));
            transformer.transform(source, result);

            System.out.println("Done");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String createXml(List<FtFirst> ftTrans) {
        String filePath = null;
        try {

            File fXmlFile = new File("E:\\xml\\1.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
//            Node company = doc.getFirstChild();
            Node staff = doc.getElementsByTagName("*").item(0);
            NodeList list = staff.getChildNodes();

            for (int i = 0; i < list.getLength(); i++) {

                Node node = list.item(i);

                // get the salary element, and update the value
                if ("report_code".equals(node.getNodeName())) {
                    node.setTextContent("SAR");
                }
                if ("entity_reference".equals(node.getNodeName())) {
                    node.setTextContent("16001");
                }

                if ("submission_date".equals(node.getNodeName())) {
                    node.setTextContent("today");
                }
                if ("currency_code_local".equals(node.getNodeName())) {
                    node.setTextContent("USD");
                }

            }
            NodeList nList3 = doc.getElementsByTagName("*");
            for (int temp = 0; temp < nList3.getLength(); temp++) {

                Node nNode = nList3.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    System.out.println("aaaaaaaa" + nNode.getNodeName());
                    if ("gender".equals(nNode.getNodeName())) {

                        nNode.setTextContent("100220000");
                    }
                    if ("title".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("first_name".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("last_name".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("birthdate".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("ssn".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }
                    if ("nationality1".equals(nNode.getNodeName())) {

                        nNode.setTextContent("CC");
                    }

                }
            }
            Node tph_contact_type = doc.getElementsByTagName("tph_contact_type").item(0);
            tph_contact_type.setTextContent("qqq");

            Node tph_communication_type = doc.getElementsByTagName("tph_communication_type").item(0);
            tph_communication_type.setTextContent("qqq");

            Node tph_number = doc.getElementsByTagName("tph_number").item(0);
            tph_number.setTextContent("qqq");

            Node address_type = doc.getElementsByTagName("address_type").item(0);
            address_type.setTextContent("qqq");

            Node city = doc.getElementsByTagName("city").item(0);
            city.setTextContent("qqq");

            Node country_code = doc.getElementsByTagName("country_code").item(0);
            country_code.setTextContent("qqq");

            Node state = doc.getElementsByTagName("state").item(0);
            state.setTextContent("qqq");

            Node occupation = doc.getElementsByTagName("occupation").item(0);
            occupation.setTextContent("occupation");

            Node address_type2 = doc.getElementsByTagName("address_type").item(1);
            address_type2.setTextContent("address_type22");

            Node address = doc.getElementsByTagName("address").item(1);
            address.setTextContent("addresssss");

            NodeList nList = doc.getElementsByTagName("transactionnumber");

            System.out.println("----------------------------");
            XmlUtils utils = new XmlUtils();
//            utils.addXmlTrans(ftTrans, doc);
//            Node transactionnumber = doc.getElementsByTagName("transactionnumber").item(0);
//            transactionnumber.setTextContent("qqq");
//            Node transaction_location = doc.getElementsByTagName("transaction_location").item(0);
//            transaction_location.setTextContent("qqq");
//            Node transaction_description = doc.getElementsByTagName("transaction_description").item(0);
//            transaction_description.setTextContent("transaction_description");
//            Node date_transaction = doc.getElementsByTagName("date_transaction").item(0);
//            date_transaction.setTextContent("date_transaction");
//            Node teller = doc.getElementsByTagName("teller").item(0);
//            teller.setTextContent("teller");
//            Node authorized = doc.getElementsByTagName("authorized").item(0);
//            authorized.setTextContent("authorized");
//            Node transmode_code = doc.getElementsByTagName("transmode_code").item(0);
//            transmode_code.setTextContent("transmode_code");
//            Node amount_local = doc.getElementsByTagName("amount_local").item(0);
//            amount_local.setTextContent("amount_local");
//            for (int i = 0; i < nList.getLength(); i++) {
//                Node reportn = nList.item(i);
//                if (reportn.getNodeType() == Node.ELEMENT_NODE) {
//
//                    System.out.println("mmmmmmmmmm" + reportn.getNodeName());
//                    if ("transactionnumber".equals(reportn.getNodeName())) {
//
//                        reportn.setTextContent("qqq");
//                    }
//                }
//            }
//            for (int temp = 0; temp < nList.getLength(); temp++) {
//
//                Node nNode = nList.item(temp);
//
//                System.out.println("\nCurrent Element :" + nNode.getNodeName());
//
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    System.out.println("aaaaaaaa" + nNode.getNodeName());
//                    if ("transactionnumber".equals(nNode.getNodeName())) {
//
//                        nNode.setTextContent("11111");
//                    }
//                }
//            }
//            NodeList nList2 = doc.getElementsByTagName("amount_local");
//            for (int temp = 0; temp < nList2.getLength(); temp++) {
//
//                Node nNode = nList2.item(temp);
//
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    System.out.println("aaaaaaaa" + nNode.getNodeName());
//                    if ("amount_local".equals(nNode.getNodeName())) {
//
//                        nNode.setTextContent("00000");
//                    }
//                }
//            }
            filePath = "E:\\xml\\1.xml";
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filePath));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

}
