/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Mahmoud
 */
public class Testing {

    public static void main(String[] args) {
      
        Testing t =new Testing();
        String s =t.getDate("20170101");
        System.out.println(""+s);
    }
    
    public String  getDate(String date){
          String s=date;
        String year=s.substring(0, 4);
        String month=s.substring(4, 6);
        String day=s.substring(6, 8);
        String dat=year+"-"+month+"-"+day;
        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss");
        DateTime jodaTime = DateTime.parse(dat);
      
        return jodaTime.toString(dateStringFormat);
    }
}
