/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

/**
 *
 * @author Mahmoud
 */
import java.io.File;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class DOM4JParserDemo {

    public static void main(String[] args) {
        try {
            File inputFile = new File("E:\\xml\\1.xml");
            SAXReader reader = new SAXReader();
            Document document = reader.read(inputFile);
            System.out.println("Root element :"
                    + document.getRootElement().getName());

            Element classElement = document.getRootElement();

            List<Node> nodes = document.selectNodes("/report/transaction");
            
            System.out.println("----------------------------");
            for (Node node : nodes) {
                System.out.println("\nCurrent Element :"
                        + node.getName());
//            System.out.println("Student roll no : " 
//               + node.valueOf("@rollno") );
                
                System.out.println("First Name : " + node.selectSingleNode("transactionnumber").getText());
                System.out.println("Last Name : " + node.selectSingleNode("transaction_location").getText());
            }
           
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
