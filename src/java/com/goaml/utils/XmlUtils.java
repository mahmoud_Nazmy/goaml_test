/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.utils;

import com.goaml.beans.UsersBean;
import com.goaml.xml.Location;
import com.goaml.xml.ReportingPerson;
import com.goaml.xml.addresses;
import com.goaml.xml.phones;
import com.goaml.xml.report;
import com.goaml.xml.report_indicators;
import org.joda.time.DateTime;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Mahmoud
 */
public class XmlUtils {

    public XmlUtils() {
    }

    public static void main(String[] args) {
        try {

            JAXBContext jc = JAXBContext.newInstance(report.class);

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            File xml = new File("E:\\xml\\1000178526\\1000178526.xml");
            report tests = (report) unmarshaller.unmarshal(xml);

            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(tests, System.out);;

        } catch (JAXBException e) {
            System.out.println("Erro::" + e.getMessage());
            e.printStackTrace();
        }
    }

    public report createRepHeader(String repId, String reason, String action, String repType) {
        DateTime dt = DateTime.now();

        report r = new report();
        r.setRentity_id("TEST");
        r.setSubmission_code("E");
        r.setReport_code(repType);
        r.setEntity_reference(repId);
        r.setSubmission_date(dt.toString());
        r.setCurrency_code_local("EGP");
        r.setReason(reason);
        r.setAction(action);
        return r;
    }

    public Location createLocation(String Address_type, String Address, String city, String countryCode, String state) {
        Location location = new Location();
        location.setAddress(Address);
        location.setAddress_type(Address_type);
        location.setState(state);
        location.setCity(city);
        location.setCountry_code(countryCode);
        return location;
    }

    public ReportingPerson addReportingPerson(UsersBean user, List<phones> phonesList, List<addresses> addressList) {
        ReportingPerson reportingPerson = new ReportingPerson();
        reportingPerson.setAddresses(addressList);
        reportingPerson.setPhones(phonesList);
        reportingPerson.setGender(user.getUsers().getGender());
        reportingPerson.setTitle(user.getUsers().getTitle());
        reportingPerson.setFirst_name(user.getUsers().getFirstName());
        reportingPerson.setLast_name(user.getUsers().getLastName());
        reportingPerson.setBirthdate(user.getUsers().getBirthDate());
        reportingPerson.setSsn(user.getUsers().getNationalId());
        reportingPerson.setAddresses(addressList);
        reportingPerson.setPhones(phonesList);
        reportingPerson.setEmail(user.getUsers().getEmail());
        reportingPerson.setOccupation(user.getUsers().getOccupation());
        return reportingPerson;
    }

    public report_indicators addReportIndicators(List<String> indicators) {
        report_indicators indicator = new report_indicators();
        List<String> indicatorsList = null;
//        for (int i = 0; i < indicators.size(); i++) {
//            indicatorsList = new ArrayList<>();
//            indicatorsList.add(indicators.get(i));
//        }
        indicator.setIndicator(indicatorsList);

        return indicator;
    }

    public String checkTransType(String transRef) {
        String type = "";
        if (transRef.substring(0, 1).equalsIgnoreCase("TT")) {
            type = "TT";

        } else if (transRef.substring(0, 1).equalsIgnoreCase("FT")) {
            type = "FT";
        }
        return type;
    }

    public String[] getNames(String fullName) {
        String[] names = fullName.split("\\s+");
        return names;
    }

    public String formatDate(String date) {
        String s = date;
        String year = s.substring(0, 4);
        String month = s.substring(4, 6);
        String day = s.substring(6, 8);
        String dat = year + "-" + month + "-" + day;
        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss");
        DateTime jodaTime = DateTime.parse(dat);

        return jodaTime.toString(dateStringFormat);
    }
}
