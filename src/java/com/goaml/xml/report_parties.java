/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Mahmoud
 */
public class report_parties {
    private List<report_party> reportParty;

    
    @XmlElement(name = "report_party")
    
    public List<report_party> getReportParty() {
        return reportParty;
    }

    public void setReportParty(List<report_party> reportParty) {
        this.reportParty = reportParty;
    }

  
    
}
