/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"to_funds_code", "to_account","to_country"})
public class t_to_my_client {

    private String to_funds_code;
    private to_account to_account = new to_account();
    private String to_country ;
    

    public String getTo_funds_code() {
        return to_funds_code;
    }

    public void setTo_funds_code(String to_funds_code) {
        this.to_funds_code = to_funds_code;
    }

    public to_account getTo_account() {
        return to_account;
    }

    public void setTo_account(to_account to_account) {
        this.to_account = to_account;
    }

    public String getTo_country() {
        return to_country;
    }

    public void setTo_country(String to_country) {
        this.to_country = to_country;
    }

   


   
    
}
