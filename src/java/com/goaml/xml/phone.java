/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"tph_contact_type", "tph_communication_type", "tph_number"})
public class phone {

    private String tph_contact_type;
    private String tph_communication_type;
    private String tph_number;

    public String getTph_contact_type() {
        return tph_contact_type;
    }

    public void setTph_contact_type(String tph_contact_type) {
        this.tph_contact_type = tph_contact_type;
    }

    public String getTph_communication_type() {
        return tph_communication_type;
    }

    public void setTph_communication_type(String tph_communication_type) {
        this.tph_communication_type = tph_communication_type;
    }

    public String getTph_number() {
        return tph_number;
    }

    public void setTph_number(String tph_number) {
        this.tph_number = tph_number;
    }
}
