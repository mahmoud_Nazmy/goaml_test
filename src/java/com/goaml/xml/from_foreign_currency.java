/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"foreign_currency_code", "foreign_amount", "foreign_exchange_rate"})
public class from_foreign_currency {
    private String foreign_currency_code;
    private String foreign_amount;
    private String foreign_exchange_rate;

    public String getForeign_currency_code() {
        return foreign_currency_code;
    }

    public void setForeign_currency_code(String foreign_currency_code) {
        this.foreign_currency_code = foreign_currency_code;
    }

    public String getForeign_amount() {
        return foreign_amount;
    }

    public void setForeign_amount(String foreign_amount) {
        this.foreign_amount = foreign_amount;
    }

    public String getForeign_exchange_rate() {
        return foreign_exchange_rate;
    }

    public void setForeign_exchange_rate(String foreign_exchange_rate) {
        this.foreign_exchange_rate = foreign_exchange_rate;
    }
    
}
