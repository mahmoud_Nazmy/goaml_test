/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"from_funds_code", "from_person", "identification", "from_country"})
public class t_from {

    private String from_funds_code;
    private String from_country;

    private List<ReportingPerson> from_person;
    private List<identification> identification;

    public String getFrom_funds_code() {
        return from_funds_code;
    }

    public void setFrom_funds_code(String from_funds_code) {
        this.from_funds_code = from_funds_code;
    }

    public String getFrom_country() {
        return from_country;
    }

    public void setFrom_country(String from_country) {
        this.from_country = from_country;
    }

    @XmlElement(name = "from_person")
    public List<ReportingPerson> getFrom_person() {
        return from_person;
    }

    public void setFrom_person(List<ReportingPerson> from_person) {
        this.from_person = from_person;
    }

    public List<identification> getIdentification() {
        return identification;
    }

    public void setIdentification(List<identification> identification) {
        this.identification = identification;
    }

}
