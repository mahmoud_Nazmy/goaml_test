/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"institution_name", "swift", "non_bank_institution", "branch", "account", "currency_code","personal_account_type","signatory","opened","balance","date_balance","status_code"})
public class from_account {

    private String institution_name;
    private String swift;
    private String non_bank_institution;
    private String branch;
    private String account;
    private String currency_code;
    private String personal_account_type;
    private String opened;
    private String balance;
    private String date_balance;
    private String status_code;
    private List<signatory> signatory;

    public String getInstitution_name() {
        return institution_name;
    }

    public void setInstitution_name(String institution_name) {
        this.institution_name = institution_name;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public String getNon_bank_institution() {
        return non_bank_institution;
    }

    public void setNon_bank_institution(String non_bank_institution) {
        this.non_bank_institution = non_bank_institution;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getPersonal_account_type() {
        return personal_account_type;
    }

    public void setPersonal_account_type(String personal_account_type) {
        this.personal_account_type = personal_account_type;
    }

    public String getOpened() {
        return opened;
    }

    public void setOpened(String opened) {
        this.opened = opened;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDate_balance() {
        return date_balance;
    }

    public void setDate_balance(String date_balance) {
        this.date_balance = date_balance;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public List<signatory> getSignatory() {
        return signatory;
    }

    public void setSignatory(List<signatory> signatory) {
        this.signatory = signatory;
    }
    
}
