/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"transaction", "transactionnumber", "transaction_location", "transaction_description", "date_transaction", "teller", "authorized", "transmode_code", "amount_local", "t_from", "t_from_my_client","t_to_my_client"})

public class Transaction {

    private String transaction;
    private String transactionnumber;
    private String transaction_location;
    private String transaction_description;
    private String date_transaction;
    private String teller;
    private String authorized;
    private String transmode_code;
    private String amount_local;
    private t_from t_from = new t_from();
    private t_from_my_client t_from_my_client = new t_from_my_client();
    private t_to_my_client t_to_my_client = new t_to_my_client();

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getTransactionnumber() {
        
        return transactionnumber;
    }

    public void setTransactionnumber(String transactionnumber) {
        this.transactionnumber = transactionnumber;
    }

    public String getTransaction_location() {
        return transaction_location;
    }

    public void setTransaction_location(String transaction_location) {
        this.transaction_location = transaction_location;
    }

    public String getTransaction_description() {
        return transaction_description;
    }

    public void setTransaction_description(String transaction_description) {
        this.transaction_description = transaction_description;
    }

    public String getDate_transaction() {
        return date_transaction;
    }

    public void setDate_transaction(String date_transaction) {
        this.date_transaction = date_transaction;
    }

    public String getTeller() {
        return teller;
    }

    public void setTeller(String teller) {
        this.teller = teller;
    }

    public String getAuthorized() {
        return authorized;
    }

    public void setAuthorized(String authorized) {
        this.authorized = authorized;
    }

    public String getTransmode_code() {
        return transmode_code;
    }

    public void setTransmode_code(String transmode_code) {
        this.transmode_code = transmode_code;
    }

    public String getAmount_local() {
        return amount_local;
    }

    public void setAmount_local(String amount_local) {
        this.amount_local = amount_local;
    }

    public t_from getT_from() {
        return t_from;
    }

    public void setT_from(t_from t_from) {
        this.t_from = t_from;
    }

    public t_from_my_client getT_from_my_client() {
        return t_from_my_client;
    }

    public void setT_from_my_client(t_from_my_client t_from_my_client) {
        this.t_from_my_client = t_from_my_client;
    }

    public t_to_my_client getT_to_my_client() {
        return t_to_my_client;
    }

    public void setT_to_my_client(t_to_my_client t_to_my_client) {
        this.t_to_my_client = t_to_my_client;
    }

}
