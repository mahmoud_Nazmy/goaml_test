/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"name", "commercial_name", "incorporation_legal_form","incorporation_number","business","phones","addresses","incorporation_country_code","director_id","incorporation_date","tax_number"})
public class t_entity {
    private String name;
    private String commercial_name;
    private String incorporation_legal_form;
    private String incorporation_number;
    private String business;
    private List<phones> phones = new ArrayList<>();
    private List<addresses> addresses = new ArrayList<>();
    private String incorporation_country_code;
    private List<director_id> director_id;
    private String incorporation_date;
    private String tax_number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommercial_name() {
        return commercial_name;
    }

    public void setCommercial_name(String commercial_name) {
        this.commercial_name = commercial_name;
    }

    public String getIncorporation_legal_form() {
        return incorporation_legal_form;
    }

    public void setIncorporation_legal_form(String incorporation_legal_form) {
        this.incorporation_legal_form = incorporation_legal_form;
    }

    public String getIncorporation_number() {
        return incorporation_number;
    }

    public void setIncorporation_number(String incorporation_number) {
        this.incorporation_number = incorporation_number;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public List<phones> getPhones() {
        return phones;
    }

    public void setPhones(List<phones> phones) {
        this.phones = phones;
    }

    public List<addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<addresses> addresses) {
        this.addresses = addresses;
    }

    public String getIncorporation_country_code() {
        return incorporation_country_code;
    }

    public void setIncorporation_country_code(String incorporation_country_code) {
        this.incorporation_country_code = incorporation_country_code;
    }

    public List<director_id> getDirector_id() {
        return director_id;
    }

    public void setDirector_id(List<director_id> director_id) {
        this.director_id = director_id;
    }

    public String getIncorporation_date() {
        return incorporation_date;
    }

    public void setIncorporation_date(String incorporation_date) {
        this.incorporation_date = incorporation_date;
    }

    public String getTax_number() {
        return tax_number;
    }

    public void setTax_number(String tax_number) {
        this.tax_number = tax_number;
    }
    
}
