/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Mahmoud
 */
public class Transactions {
      @XmlElement
    private List <Transaction>transactions; 

    public List <Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List <Transaction> transactions) {
        this.transactions = transactions;
    }
      
}
