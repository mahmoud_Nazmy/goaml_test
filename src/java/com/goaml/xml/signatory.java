/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"is_primary", "t_person", "role"})
public class signatory {

    private String is_primary;
    private List<t_person> t_person;
    private String role;

    public String getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(String is_primary) {
        this.is_primary = is_primary;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @XmlElement(name = "t_person")
    public List<t_person> getT_person() {
        return t_person;
    }

    public void setT_person(List<t_person> t_person) {
        this.t_person = t_person;
    }

}
