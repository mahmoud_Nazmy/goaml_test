/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;

/**
 *
 * @author Mahmoud
 */
public class director_id extends ReportingPerson {

    private List<identification> identification;

    public List<identification> getIdentification() {
        return identification;
    }

    public void setIdentification(List<identification> identification) {
        this.identification = identification;
    }
    
}
