/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"reporting_person", "gender", "title", "first_name", "middle_name", "prefix", "last_name", "birthdate", "ssn", "nationality1", "residence", "phones", "addresses", "email", "occupation"})
public class ReportingPerson {

    private String reporting_person;
    private String gender;
    private String title;
    private String first_name;
    private String middle_name;
    private String prefix;
    private String last_name;
    private String birthdate;
    private String ssn;
    private String nationality1;
    private List<phones> phones = new ArrayList<>();
    private List<addresses> addresses = new ArrayList<>();
    private String email;
    private String occupation;
    private String residence;

    @XmlElement(name = "reporting_person")
    public String getReporting_person() {
        return reporting_person;
    }

    public void setReporting_person(String reporting_person) {
        this.reporting_person = reporting_person;
    }

    @XmlElement(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @XmlElement(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name = "first_name")
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    @XmlElement(name = "last_name")
    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    @XmlElement(name = "birthdate")
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    @XmlElement(name = "ssn")
    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @XmlElement(name = "nationality1")
    public String getNationality1() {
        return nationality1;
    }

    public void setNationality1(String nationality1) {
        this.nationality1 = nationality1;
    }

    @XmlElement(name = "phones")
    public List<phones> getPhones() {
        return phones;
    }

    public void setPhones(List<phones> phones) {
        this.phones = phones;
    }

    @XmlElement(name = "addresses")
    public List<addresses> getAddresses() {

        return addresses;
    }

    public void setAddresses(List<addresses> addresses) {
        this.addresses = addresses;
    }

    @XmlElement(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlElement(name = "occupation")
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

}
