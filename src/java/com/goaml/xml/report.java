/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlRootElement(name = "report")
@XmlType(propOrder = {"rentity_id", "submission_code", "report_code", "entity_reference", "submission_date", "currency_code_local", "reporting_person", "location", "reason", "action","activity","transaction", "report_indicators"})
public class report {

    private String rentity_id;
    private String submission_code;
    private String report_code;
    private String entity_reference;
    private String submission_date;
    private String currency_code_local;
    private String reason;
    private String action;
//    @XmlAttribute
//    private String reporting_person;
//    @XmlValue
//     private String gender;

    private List<Transaction> transaction = new ArrayList<>();
    private List<activity> activity = new ArrayList<>();
    private List<report_indicators> report_indicators = new ArrayList<>();

//    private List<phones> phones = new ArrayList<>();
    private ReportingPerson reporting_person = new ReportingPerson();
    private List<Location> location = new ArrayList<>();

    public String getRentity_id() {
        return rentity_id;
    }

    @XmlElement
    public void setRentity_id(String rentity_id) {
        this.rentity_id = rentity_id;
    }

    public String getSubmission_code() {
        return submission_code;
    }

    @XmlElement
    public void setSubmission_code(String submission_code) {
        this.submission_code = submission_code;
    }

    public String getReport_code() {
        return report_code;
    }

    @XmlElement
    public void setReport_code(String report_code) {
        this.report_code = report_code;
    }

    public String getEntity_reference() {
        return entity_reference;
    }

    @XmlElement
    public void setEntity_reference(String entity_reference) {
        this.entity_reference = entity_reference;
    }

    public String getSubmission_date() {
        return submission_date;
    }

    @XmlElement
    public void setSubmission_date(String submission_date) {
        this.submission_date = submission_date;
    }

    public String getCurrency_code_local() {
        return currency_code_local;
    }

    @XmlElement
    public void setCurrency_code_local(String currency_code_local) {
        this.currency_code_local = currency_code_local;
    }

    @XmlElement(name = "transaction")
    public List<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }

    public ReportingPerson getReporting_person() {
        return reporting_person;
    }

    public void setReporting_person(ReportingPerson reporting_person) {
        this.reporting_person = reporting_person;
    }

    @XmlElement(name = "reason")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @XmlElement(name = "action")
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @XmlElement(name = "report_indicators")
    public List<report_indicators> getReport_indicators() {
        return report_indicators;
    }

    public void setReport_indicators(List<report_indicators> report_indicators) {
        this.report_indicators = report_indicators;
    }

    @XmlElement(name = "location")
    public List<Location> getLocation() {
        return location;
    }

    public void setLocation(List<Location> location) {
        this.location = location;
    }
    @XmlElement(name = "activity")
    public List<activity> getActivity() {
        return activity;
    }

    public void setActivity(List<activity> activity) {
        this.activity = activity;
    }
    
}
