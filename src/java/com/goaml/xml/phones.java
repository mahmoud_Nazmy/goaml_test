/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
public class phones {

    private List<phone> phone;

    @XmlElement(name = "phone")
    public List<phone> getPhone() {
        return phone;
    }

    public void setPhone(List<phone> phone) {
        this.phone = phone;
    }

}
