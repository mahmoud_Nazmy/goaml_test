/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goaml.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Mahmoud
 */
@XmlType(propOrder = {"from_funds_code", "from_person",  "from_country"})
public class t_from_my_client {

    private String from_funds_code;
    private List<from_person> from_person;

    private String from_country;

    public String getFrom_funds_code() {
        return from_funds_code;
    }

    public void setFrom_funds_code(String from_funds_code) {
        this.from_funds_code = from_funds_code;
    }

   

    public String getFrom_country() {
        return from_country;
    }

    public void setFrom_country(String from_country) {
        this.from_country = from_country;
    }

    @XmlElement(name = "from_person")
    public List<from_person> getFrom_person() {
        return from_person;
    }

    public void setFrom_person(List<from_person> from_person) {
        this.from_person = from_person;
    }

}
